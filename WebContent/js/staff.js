
function add(){

	var url = 'addStaff?name=' + $('#newName').val();
	url += "&newId=" + $('#newId').val();
	if($('#newFlag').prop('checked')){
		url += '&flag=true';
	}
	location.href=url;
}


function updateSend(num){
	var url = 'updateStaff?id=' + num;
	url+='&name=' + $('#name'+num).val();
	url+='&newId=' + $('#id'+num).val();
	
	if($('#flag'+num).prop('checked')){
		url += '&flag=true';
	}
	
	location.href=url;
}

function cancel(num){
	// 名前部分
	var content = $('<div></div>');
	$(content).attr('id', 'name'+num);
	$(content).attr('value', $('#name'+num).attr('value'));
	$(content).append($('#name'+num).attr('value'));
	$('#name'+num).replaceWith(content);

	// ID部分
	content = $('<div></div>');
	$(content).attr('id', 'id'+num);
	$(content).attr('value', $('#id'+num).attr('value'));
	$(content).append($('#id'+num).attr('value'));
	$('#id'+num).replaceWith(content);

	// チェックボックス
	content = $('<div></div>');
	$(content).attr('id', 'flag'+num);
	if($('#flag'+num).val() == 'true'){
		$(content).append('あり');
		$(content).attr('value', 'true');
	}else{
		$(content).append('なし');
		$(content).attr('value', 'false');	
	}
	$('#flag'+num).replaceWith(content);
	
	// ボタン
	content = $('<button></button>');
	$(content).append('編集');
	$(content).attr('id', 'edit' + num);
	$(content).attr('onclick', 'updateStart(' + num + ')');
	$('#update'+num).replaceWith(content);
	$('#cancel'+num).remove();
}


function updateStart(num){
	// 名前部分
	var content = $('<input type="text" id="name' + num + '">');
	$(content).attr('value', $('#name'+num).attr('value'));
	$(content).attr('maxlength', 30);
	$('#name'+num).replaceWith(content);

	// ID部分
	content = $('<input type="text" id="id' + num + '">');
	$(content).attr('value', $('#id'+num).attr('value'));
	$(content).attr('maxlength', 30);
	$('#id'+num).replaceWith(content);

	// チェックボックス
	content = $('<input type="checkbox" id="flag' + num + '">');
	if($('#flag'+num).attr('value') == 'true'){
		$(content).prop('checked',true);
		$(content).attr('value','true');
	}else{
		$(content).attr('value','false');
	}
	$('#flag'+num).replaceWith(content);
	
	// ボタン
	//content = $('<button type="button" id="update' + num + '" onclick="sendUpdate(' + num + ')>更新</button>');
	content = $('<button></button>');
	$(content).append('更新');
	$(content).attr('id', 'update' + num);
	$(content).attr('onclick', 'updateSend(' + num + ')');
	$('#edit'+num).replaceWith(content);
	
	var cancel = $('<button></button>')
	$(cancel).append('キャンセル');
	$(cancel).attr('id', 'cancel' + num);
	$(cancel).attr('onclick', 'cancel(' + num + ')');
	$('#update'+num).after(cancel);
	
}


