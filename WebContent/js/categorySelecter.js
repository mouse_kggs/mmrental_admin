var $genreBox = $('.genreBox');
var original = $genreBox.html();

document.addEventListener('DOMContentLoaded',function(){
	$genreBox = $('.genreBox');
	original = $genreBox.html();
	//setGenreBoxDisable();
	//categoryChanged();
	var timer = 0;
	var q = setInterval(function(){
		timer++;
		if(timer>=2){
			categoryChanged();
			clearInterval(q);
		}
	},50);
});

function setGenreBoxDisable(){
	  var obj = document.test.categorySelectBox;
	  var index = obj.value;
	  var $elementReference = document.getElementById("genreSelectBox");
	  if (index == 0){
		  $elementReference.disabled = true;
		  $elementReference.value = 0;
	  }else{
		  $elementReference.disabled = false;
	  }
}

function categoryChanged(){
	 // var obj = document.test.categorySelectBox;
	  var obj = document.getElementById("categorySelectBox");
	  var index = obj.value;
	  var $elementReference = document.getElementById("genreSelectBox");
	  if (index == 0){
		  $elementReference.disabled = true;
		  $elementReference.value = 0;
	  }else{
		  $elementReference.disabled = false;
		  $genreBox.html(original).find('option').each(function(){
			  var val2 = $(this).attr('class');
			  if(index != val2){
				  $(this).not(':first-child').remove();
			  }
		  });
	  }
}