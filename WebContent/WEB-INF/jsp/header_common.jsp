<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@
	page import="dto.GenreDTO, java.util.ArrayList, dao.GenreDAO"
%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<script type="text/javascript" src="js/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="js/searchpage.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>

<form style="float:left" action="/MMRental_Admin/control" method="post">
	<input type="hidden" name="action" value="rentalAll">
	<input type="submit" value="レンタル一覧"/>
</form>
<c:if test="${login_dto.manager_flg == true}">
<form style="float:left" action="/MMRental_Admin/control" method="post">
	<input type="hidden" name="action" value="itemMenu">
	<input type="submit" value="商品マスタメンテナンス"/>
</form>
<form style="float:left" action="/MMRental_Admin/control" method="post">
	<input type="hidden" name="action" value="staffMenu">
	<input type="submit" value="従業員マスタメンテナス"/>
</form>
<form style="float:left" action="/MMRental_Admin/analyze" method="get">
	<input type="submit" value="分析"/>
</form>
</c:if>
<form style="float:right" action="/MMRental_Admin/control" method="post">
	<input type="hidden" name="action" value="logout">
	<input type="submit" value="ログアウト"/>
</form>
<br>

</body>
</html>