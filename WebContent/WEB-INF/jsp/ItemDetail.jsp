<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%@
	page import="dto.GenreDTO, java.util.ArrayList, dao.GenreDAO"
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script type="text/javascript" src="js/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="js/categorySelecter.js"></script>
<title>商品詳細</title>
<link rel="stylesheet" type="text/css" href="css/item.css">
</head>
<body>
<jsp:include page="header_common.jsp" flush="true" /><br>
<%
	request.setAttribute("categoryList", GenreDAO.selectCategoryAllStatic());
	request.setAttribute("genreList", GenreDAO.selectGenreAllStatic());
%>
<h2>商品詳細</h2><hr>
<c:if test="${actionName == 'itemAddScene' || actionName == 'itemAdd'}">
<form action="/MMRental_Admin/control" method="POST" enctype="multipart/form-data">
	<input type="hidden" name="action" value="itemAdd">
	<input type="hidden" name="id" value="${item.id}">
	<font color="ff0000">*</font>必須入力<br>
	<font color="ff0000"><c:if test="${errorMsg != null}">${errorMsg}</c:if></font>
	<table border="1" style="border-collapse: collapse; float:left">
		<tr><th bgcolor="#FFBDFF">
			<font color="ff0000">*</font>新旧区分</th>
		<td><div align="center">
		<select name="newAndOld">
			<option value="1"<c:if test="${item.newAndOldId == 1}">
			<c:out value="selected"/></c:if>>新作</option>
			<option value="2"<c:if test="${item.newAndOldId == 2}">
			<c:out value="selected"/></c:if>>準新作</option>
			<option value="3"<c:if test="${item.newAndOldId == 3}">
			<c:out value="selected"/></c:if>>旧作</option>
		</select></div></td></tr>
		<tr><th bgcolor="#FFBDFF">
			<font color="ff0000">*</font>カテゴリ</th><td><div align="center">
			<select id="categorySelectBox" class="categoryBox" name="category" onChange="categoryChanged()">
				<option value="0" <c:if test="${searchCategory == 0}"><c:out value="selected"/></c:if>>カテゴリ</option>
				<c:forEach var="category" items="${categoryList}">
					<option value='<c:out value="${category.category_id}"/>' <c:if test="${item.categoryId == category.category_id}"><c:out value="selected"/></c:if>><c:out value="${category.category_name}"/></option>
				</c:forEach>
			</select></div></td></tr>
		<tr><th bgcolor="#FFBDFF">
			<font color="ff0000">*</font>ジャンル</th><td><div align="center">
			<select id="genreSelectBox" class="genreBox" name="genre">
				<option value="0" >ジャンル</option>
				<c:forEach var="genre" items="${genreList}">
					<option class='<c:out value="${genre.category_id}"/>'
							value='<c:out value="${genre.genre_id}"/>'
							<c:if test="${item.genreId == genre.genre_id}"
								><c:out value="selected"/>
							</c:if>><c:out value="${genre.genre_name}"/></option>
				</c:forEach>
			</select>
		</div></td></tr>
		<tr><th bgcolor="#FFBDFF">
			<font color="ff0000">*</font>タイトル</th>
		<td><input type="text" name="title" value="${item.name}"/></td></tr>
		<tr><th bgcolor="#FFBDFF">
			<font color="ff0000">*</font>アーティスト/監督</th>
		<td><input type="text" name="artist" value="${item.artist}"></td></tr>
		<tr><th bgcolor="#FFBDFF">
			<font color="ff0000">*</font>値段</th>
		<td><div align="right"><input type="text" name="price" value="${item.price}"></div></td></tr>
		<tr><th bgcolor="#FFBDFF">
			<font color="ff0000">*</font>採番番号</th>
		<td><div align="right"><input type="text" name="maxIdentNum" value="${item.maxIdentificationNum}"></div></td></tr>
		<tr><th bgcolor="#FFBDFF">
			<font color="ff0000">*</font>おすすめ</th>
		<td><div align="center">
			<input type="checkbox" name="recommend" value="1">
		</div></td></tr>
		<tr><th bgcolor="#FFBDFF">その他</th><td><div align="right"><input type="text" name="remarks" value="${item.remarks}"></div></td></tr>
	</table>
	<table border="1" style="border-collapse: collapse; float:left">
		<tr>
			<th bgcolor="#FFBDFF">商品画像</th>
			<td><div align="center"><img style="width: 100px; height: 100px" src="image?id=${item.id}"></div></td>
		</tr>
		<tr>
			<td colspan="2"><input type="file" name="file"></td>
		</tr>
	</table>
	<br><br><br><br><br><br><br><br><br><br><br>
	<input type="submit" value="登録">
</form>
</c:if>
<c:if test="${actionName == 'itemDetail' || actionName == 'disposal' || actionName == 'itemEdit' || actionName == 'disposalCancel'}">
<form action="/MMRental_Admin/control" method="POST" enctype="multipart/form-data">
	<input type="hidden" name="action" value="itemEdit">
	<font color="ff0000">*</font>必須入力<br>
	<font color="ff0000"><c:if test="${errorMsg != null}">${errorMsg}</c:if></font>
	<table border="1" style="border-collapse: collapse; float:left">
		<tr><th bgcolor="#FFBDFF">
			<font color="ff0000">*</font>商品番号</th>
		<td><div align="right"><input type="text" name="id" value="${item.id}"></div></td></tr>
		<tr><th bgcolor="#FFBDFF">
			<font color="ff0000">*</font>新旧区分</th>
		<td><div align="center">
		<select name="newAndOld">
			<option value="1"<c:if test="${item.newAndOld == '新作'}">
			<c:out value="selected"/></c:if>>新作</option>
			<option value="2"<c:if test="${item.newAndOld == '準新作'}">
			<c:out value="selected"/></c:if>>準新作</option>
			<option value="3"<c:if test="${item.newAndOld == '旧作'}">
			<c:out value="selected"/></c:if>>旧作</option>
		</select></div></td></tr>
		<tr><th bgcolor="#FFBDFF">
			<font color="ff0000">*</font>カテゴリ</th><td><div align="center">
			<select id="categorySelectBox" class="categoryBox" name="category" onChange="categoryChanged()">
				<option value="0" <c:if test="${searchCategory == 0}"><c:out value="selected"/></c:if>>カテゴリ</option>
				<c:forEach var="category" items="${categoryList}">
					<option value='<c:out value="${category.category_id}"/>' <c:if test="${item.categoryId == category.category_id}"><c:out value="selected"/></c:if>><c:out value="${category.category_name}"/></option>
				</c:forEach>
			</select></div></td></tr>
		<tr><th bgcolor="#FFBDFF">
			<font color="ff0000">*</font>ジャンル</th><td><div align="center">
			<select id="genreSelectBox" class="genreBox" name="genre">
				<option value="0" >ジャンル</option>
				<c:forEach var="genre" items="${genreList}">
					<option class='<c:out value="${genre.category_id}"/>'
							value='<c:out value="${genre.genre_id}"/>'
							<c:if test="${item.genreId == genre.genre_id}"
								><c:out value="selected"/>
							</c:if>><c:out value="${genre.genre_name}"/></option>
				</c:forEach>
			</select>
		</div></td></tr>
		<tr><th bgcolor="#FFBDFF">
			<font color="ff0000">*</font>タイトル</th>
		<td><input type="text" name="title" value="${item.name}"/></td></tr>
		<tr><th bgcolor="#FFBDFF">
			<font color="ff0000">*</font>アーティスト/監督</th>
		<td><input type="text" name="artist" value="${item.artist}"></td></tr>
		<tr><th bgcolor="#FFBDFF">
			<font color="ff0000">*</font>値段</th>
		<td><div align="right"><input type="text" name="price" value="${item.price}"></div></td></tr>
		<tr><th bgcolor="#FFBDFF">
			<font color="ff0000">*</font>採番番号</th>
		<td><div align="right"><input type="text" name="maxIdentNum" value="${item.maxIdentificationNum}"></div></td></tr>
		<tr><th bgcolor="#FFBDFF">
			<font color="ff0000">*</font>おすすめ</th>
		<td><div align="center">
			<c:if test="${item.recommended == true }">
			<input type="checkbox" name="recommend" value="1"  checked></c:if>
			<c:if test="${item.recommended == false }">
			<input type="checkbox" name="recommend" value="1" ></c:if>
		</div></td></tr>
		<tr><th bgcolor="#FFBDFF">その他</th>
		<td><div align="right"><input type="text" name="remarks" value="${item.remarks}"></div></td></tr>
	</table>
	<div align="left">
	<table border="1" style="border-collapse: collapse; float:left">
		<tr>
			<th bgcolor="#FFBDFF">商品画像</th>
			<td><div align="center"><img style="width: 100px; height: 100px" src="image?id=${item.id}"></div></td>
		</tr>
		<tr>
			<td colspan="2"><input type="file" name="file"></td>
		</tr>
	</table>
	</div>
	<br><br><br><br><br><br><br><br><br><br><br><br>
		<input type="submit" value="編集確定">
	</form>
	<form action="/MMRental_Admin/control" method="POST">
		<input type="hidden" name="action" value="itemEditCancel">
		<input type="submit" value="キャンセル"><br>
	</form>

	<form action="/MMRental_Admin/control" method="POST">
		<input type="hidden" name="action" value="disposal">
		<input type="hidden" name="id" value="${item.id}">
		<input type="text" name="disNum"><input type="submit" value="廃棄">
		<c:if test="${not empty error}"><font color='ff0000'><c:out value="${error}"></c:out></font></c:if>
	</form>

	<c:if test="${not empty disposal}">

	<h2>廃番一覧</h2><hr>

	<br>
	<form action="/MMRental_Admin/control" method="POST">
	<table border="1" style="border-collapse: collapse; float:left">
		<tr>
			<th>廃番取消</th>
			<th>個体識別番号</th>
		</tr>
		<c:forEach var="disposal" items="${disposal}">
		<tr>
			<td><div align="center"><input type="checkbox" name="disNum" value="${disposal.num}"></div></td>
			<td><div align="right">${disposal.num}</div></td>
		</tr>
		</c:forEach>
	</table>
		<input type="hidden" name="action" value="disposalCancel">
		<input type="hidden" name="id" value="${item.id}">
		<input type="submit" value="取消"><br>
		<font color="#FF0000">${cancel}</font>
	</form>
	</c:if>
	<br>
</c:if>
</body>
</html>