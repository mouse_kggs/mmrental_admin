<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>レンタル一覧</title>
<link rel="stylesheet" type="text/css" href="css/rental.css">
</head>
<body>
<jsp:include page="header_common.jsp" flush="true" /><br>
<h2>レンタル一覧</h2><hr>

<form action="/MMRental_Admin/control" method="POST">
	<input type="hidden" name="action" value="rentalSearcth">
	<table>
		<tr>
			<th><font color="FFFFFF">　　　　　　</font></th>
			<th>レンタル受付日</th>
			<th>返却予定日</th>
			<th>返却日</th>
			<th><font color="FFFFFF">　</font></th>
			<th>貸出担当</th>
			<th>返却担当</th>
			<th>ステータス</th>
		</tr>
		<tr>
			<th><font color="FFFFFF">　　　　　.</font></th>
			<th><input type="text" name="orderDate" value="${history.sOrderDate}" size="22"></th>
			<th><input type="text" name="returnSchedule" value="${history.sReturnSchedule}" size="9"></th>
			<th><input type="text" name="returnDate" value="${history.sReturnDate}" size="9"></th>
			<th><font color="FFFFFF">　</font></th>
			<th>
				<select name="rentalid">
				<option value="0" <c:if test="${empty history.rentalid}">
				<c:out value="selected"/></c:if>></option>
				<c:forEach var="dto" items="${staff_dto}">
					<option value="${dto.id}" <c:if test="${dto.id == history.rentalid }">
					<c:out value="selected"/></c:if>>
						<c:out value="${dto.name}"/>
					</option>
				</c:forEach>
				</select>
			</th>
			<th><select name="returnid">
				<option value="0" <c:if test="${empty history.returnid}">
				<c:out value="selected"/></c:if>></option>
				<c:forEach var="dto" items="${staff_dto}">
					<option value="${dto.id}" <c:if test="${dto.id == history.returnid }">
					<c:out value="selected"/></c:if>>
						<c:out value="${dto.name}"/>
					</option>
				</c:forEach>
				</select></th>
			<th>
			<select name="status">
				<option value="0"<c:if test="${empty history.status}">
				<c:out value="selected"/></c:if>>
				<option value="1"<c:if test="${history.status == '受付' }">
				<c:out value="selected"/></c:if>>受付</option>
				<option value="2"<c:if test="${history.status == '延滞' }">
				<c:out value="selected"/></c:if>>延滞</option>
				<option value="3"<c:if test="${history.status == '貸出' }">
				<c:out value="selected"/></c:if>>貸出</option>
				<option value="4" <c:if test="${history.status == '返却' }">
				<c:out value="selected"/></c:if>>返却</option>
			</select></th>
			<th><center><input type="submit" value="検索"></center></th>
		</tr>
	</table>
	</form>

		<c:choose>
		<c:when test="${empty errorMsg}" >
			<table border="1"  style="border-collapse: collapse">
				<tr>
					<th bgcolor="#FFBDFF">注文受付番号</th>
					<th bgcolor="#FFBDFF">レンタル受付日</th>
					<th bgcolor="#FFBDFF">返却予定日</th>
					<th bgcolor="#FFBDFF">返却日</th>
					<th bgcolor="#FFBDFF">数量</th>
					<th bgcolor="#FFBDFF">貸出担当</th>
					<th bgcolor="#FFBDFF">返却担当</th>
					<th bgcolor="#FFBDFF">ステータス</th>
					<th bgcolor="#FFBDFF">詳細閲覧</th>
				</tr>
			<c:forEach var="dto" items="${rental_dto}" >
			<form action="/MMRental_Admin/control" method="POST">
				<input type="hidden" name="action" value="rentalDetail">

				<input type="hidden" name="id" value="<c:out value='${dto.rentalNum}'/>">
				<input type="hidden" name="orderDate" value="<c:out value='${dto.orderDate}'/>">
				<input type="hidden" name="returnSchedule" value="<c:out value='${dto.returnSchedule}'/>">
				<input type="hidden" name="returnDate" value="<c:out value='${dto.returnDate}'/>">
				<input type="hidden" name="stock" value="<c:out value='${dto.stock}'/>">
				<input type="hidden" name="rentalid" value="<c:out value='${dto.rentalid}'/>">
				<input type="hidden" name="returnid" value="<c:out value='${dto.returnid}'/>">
				<input type="hidden" name="status" value="<c:out value='${dto.status}'/>">
					<tr>
						<td><div align="right"><c:out value="${dto.rentalNum}"/></div></td>
						<td><c:out value="${dto.orderDate}"/></td>
						<td><c:out value="${dto.returnSchedule}"/></td>
						<td><c:out value="${dto.returnDate}"/></td>
						<td><div align="right"><c:out value="${dto.stock}"/></div></td>
						<td><c:out value="${dto.rentalid}"/></td>
						<td><c:out value="${dto.returnid}"/></td>
						<td><div align="center">
						<c:if test="${dto.status == '受付' }"><div id="receptionist"><c:out value="${dto.status}"/></div></c:if>
						<c:if test="${dto.status == '滞納' }"><div id="arrears"><c:out value="${dto.status}"/></div></c:if>
						<c:if test="${dto.status == '貸出' }"><div id="rental"><c:out value="${dto.status}"/></div></c:if>
						<c:if test="${dto.status == '返却' }"><div id="return"><c:out value="${dto.status}"/></div></c:if>
						</div></td>
						<td><div align="center"><input type="submit" value="詳細"/></div></td>
					</tr>
			</form>
			</c:forEach>
		</table>

		<font size="4"><center>
			<c:if test="${(page-1) > 0}"><a href='pagingRental?page=<c:out value="${page-1}"/>'>&lt; 前のページ</a></c:if>

			<c:if test="${(page-3) > 0}"><a href='pagingRental?page=<c:out value="${page-3}"/>'><c:out value="${page-3}"/></a></c:if>
			<c:if test="${(page-2) > 0}"><a href='pagingRental?page=<c:out value="${page-2}"/>'><c:out value="${page-2}"/></a></c:if>
			<c:if test="${(page-1) > 0}"><a href='pagingRental?page=<c:out value="${page-1}"/>'><c:out value="${page-1}"/></a></c:if>
			<b><c:out value="${page}"/></b>
			<c:if test="${(page+1) <= pageMax}"><a href='pagingRental?page=<c:out value="${page+1}"/>'><c:out value="${page+1}"/></a></c:if>
			<c:if test="${(page+2) <= pageMax}"><a href='pagingRental?page=<c:out value="${page+2}"/>'><c:out value="${page+2}"/></a></c:if>
			<c:if test="${(page+3) <= pageMax}"><a href='pagingRental?page=<c:out value="${page+3}"/>'><c:out value="${page+3}"/></a></c:if>

			<c:if test="${(page+1) <= pageMax}"><a href='pagingRental?page=<c:out value="${page+1}"/>'>次のページ &gt;</a></c:if>
		</center></font>
	</c:when>
		<c:otherwise><br><h2>${errorMsg}</h2></c:otherwise>
	</c:choose>

</body>
</html>