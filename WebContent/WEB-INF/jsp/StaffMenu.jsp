<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<script type="text/javascript" src="js/jquery-3.2.1.min.js"></script>
	<script type="text/javascript" src="js/staff.js" charset="UTF-8"></script>
	<style type="text/css">
		<!--
			td{text-align:center}
			th{text-align:center}
		-->
	</style>
	<title>従業員マスタメンテナンス</title>
</head>
<body>
<jsp:include page="header_common.jsp" flush="true" /><br>
<h2>従業員マスタメンテナンス</h2><hr>

<h3 style="color:red">${errorMsg}</h3>

<table border="1" style="border-collapse: collapse">
	<tr>
		<th bgcolor="#FFBDFF">氏名</th>
		<th bgcolor="#FFBDFF">従業員ID</th>
		<th bgcolor="#FFBDFF">管理者権限</th>
		<th  bgcolor="#FFBDFF" style="width:150px">情報変更</th>
		<th bgcolor="#FFBDFF">削除</th>
		<th bgcolor="#FFBDFF">パスワードリセット</th>
	</tr>
	<c:forEach var="dto" items="${staff_dto}" >
		<tr>
			<td><div id="name<c:out value='${dto.id}'/>" value="<c:out value='${dto.name}'/>"><c:out value='${dto.name}'/></div></td>
			<td><div id="id<c:out value='${dto.id}'/>" value="<c:out value='${dto.id}'/>"><c:out value='${dto.id}'/></div></td>
			<td><div id="flag<c:out value='${dto.id}'/>" value="<c:out value='${dto.manager_flg}'/>">${dto.manager_flg ? "あり":"なし"}</div></td>
			<td><button id="edit<c:out value='${dto.id}'/>" onclick="updateStart(<c:out value='${dto.id}'/>)">編集</button></td>
			<td><form method="POST" action="deleteStaff?id=<c:out value='${dto.id}'/>"><input type="submit" value="削除"></form></td>
			<td><form method="POST" action="resetPassword?id=<c:out value='${dto.id}'/>"><input type="submit" value="リセット"></form></td>
		</tr>
	</c:forEach>
	<tr>
		<td><input maxlength="30" id="newName" type="text"></input></td>
		<td><input maxlength="30" id="newId" type="text"></input></td>
		<td><input id="newFlag" type="checkbox"></input></td>
		<td><button onclick="add()">追加</button></td>
		<td></td>
		<td></td>
	</tr>
</table>



</body>
</html>