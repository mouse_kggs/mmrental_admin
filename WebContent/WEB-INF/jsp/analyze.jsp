<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@
	page import="dto.RankingDTO, java.util.ArrayList, java.sql.Date"
 %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>従業員システム ─分析─</title>
</head>
<body>
<jsp:include page="header_common.jsp" flush="true" /><br>
<h1>分析</h1>

<form action="analyze" method="post">
	<select name="category">
		<option value="1" <c:if test="${category == 1 || empty category}"><c:out value="selected"/></c:if>>CD</option>
		<option value="-1" <c:if test="${category == -1}"><c:out value="selected"/></c:if>>映像系</option>
		<option value="2" <c:if test="${category == 2}"><c:out value="selected"/></c:if>> -DVD</option>
		<option value="3" <c:if test="${category == 3}"><c:out value="selected"/></c:if>> -Bru-lay</option>
	</select>
	<select name="age">
		<option value="-1">年代</option>
		<option value="20" <c:if test="${age == 20}"><c:out value="selected"/></c:if>>～20代</option>
		<option value="30" <c:if test="${age == 30}"><c:out value="selected"/></c:if>>30代</option>
		<option value="40" <c:if test="${age == 40}"><c:out value="selected"/></c:if>>40代</option>
		<option value="50" <c:if test="${age == 50}"><c:out value="selected"/></c:if>>50代</option>
		<option value="60" <c:if test="${age == 60}"><c:out value="selected"/></c:if>>60代～</option>
	</select>
	<input type="submit" value="表示">
</form>

<table border="1" style="border-collapse: collapse">
	<tr style="background-color: LightGreen" style="text-align:center">
		<th>No</th>
		<th>タイトル</th>
		<th><c:choose><c:when test="${(category == 1) || (empty category)}"><c:out value="アーティスト"/></c:when><c:otherwise><c:out value="監督"/></c:otherwise></c:choose></th>
		<th>ジャンル</th>
		<th>レンタル数</th>
		<th>前週差</th>
		<th>前週比</th>
	</tr>
	<c:forEach var="data" items="${list}">
		<tr onmouseover="this.style.backgroundColor='#bbffff'" onmouseout="this.style.backgroundColor=''">
			<td style="text-align:center"><c:out value="${data.rank}"/></td>
			<td><c:out value="${data.title}"/></td>
			<td><c:out value="${data.artist}"/></td>
			<td><c:if test="${category == -1}"><c:out value="${data.category}/"/></c:if><c:out value="${data.genre}"/></td>
			<td style="text-align:right"><c:out value="${data.rental_num}"/></td>
			<td style="text-align:center"><c:choose><c:when test="${(data.increment > 0)}"><font color="blue">+<c:out value="${data.increment}"/></font></c:when><c:when test="${(data.increment < 0)}"><font color='red'><c:out value="${data.increment}"/></font></c:when><c:otherwise>─</c:otherwise></c:choose></td>
			<td style="text-align:right"><c:choose><c:when test="${(data.ratio > 100)}"><font color="blue"><c:out value="${data.ratio}"/>%</font></c:when><c:when test="${(data.ratio < 100) && data.ratio != 0}"><font color='red'><c:out value="${data.ratio}"/>%</font></c:when><c:otherwise>─</c:otherwise></c:choose></td>
		</tr>
	</c:forEach>

</table>

</body>
</html>