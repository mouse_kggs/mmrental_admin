<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>レンタル詳細</title>
<link rel="stylesheet" type="text/css" href="css/rental.css">
</head>
<body>
<jsp:include page="header_common.jsp" flush="true" /><br>
	<h2>レンタル詳細</h2>
	<hr>
	<table border="1">
		<tr>
			<th>貸出担当</th>
			<td>${rental_dto.rentalid}</td>
		</tr>
		<tr>
			<th>返却担当</th>
			<td>${rental_dto.returnid}</td>
		</tr>
	</table><br>
	<table border="1">
		<tr>
			<th>注文番号</th>
			<th>レンタル受付日</th>
			<th>返却予定日</th>
			<th>返却日</th>
			<th>数量</th>
			<th>ステータス</th>
		</tr>
		<tr>
			<td><div align="right">${rental_dto.rentalNum}</div></td>
			<td>${rental_dto.orderDate}</td>
			<td>${rental_dto.returnSchedule}</td>
			<td>${rental_dto.returnDate}</td>
			<td><div align="right">${rental_dto.stock}</div></td>
			<td><div align="center">
				<c:if test="${rental_dto.status == '受付'}"><div id="receptionist">${rental_dto.status}</div></c:if>
				<c:if test="${rental_dto.status == '滞納'}"><div id="arrears">${rental_dto.status}</div></c:if>
				<c:if test="${rental_dto.status == '貸出'}"><div id="rental">${rental_dto.status}</div></c:if>
				<c:if test="${rental_dto.status == '返却'}"><div id="return">${rental_dto.status}</div></c:if>
			</div></td>
		</tr>
	</table>
	<br>
	<c:if test="${rental_dto.status == '受付'}">
		<form action="/MMRental_Admin/control" method="POST">
			<input type="hidden" name="rentNum" value="${rental_dto.rentalNum}">
			<input type="hidden" name="status" value="${rental_dto.status}">
			<input type="hidden" name="action" value="rentalUpdate">
			<table border="1">
				<tr>
					<th>商品番号</th>
					<th>新旧区分</th>
					<th>カテゴリ</th>
					<th>ジャンル</th>
					<th>商品画像</th>
					<th>タイトル</th>
					<th>個体識別番号</th>
					<th>返却処理</th>
				</tr>
				<c:forEach var="dto" items="${item_dto}">
					<input type="hidden" name="itemid" value="${dto.itemid}">
					<tr>
						<td><div align="right"><c:out value="${dto.itemid}" /></div></td>
						<td><div align="center"><c:out value="${dto.newAndOld}" /></div></td>
						<td><div align="center"><c:out value="${dto.category}" /></div></td>
						<td><div align="center"><c:out value="${dto.genre}" /></div></td>
						<td><c:out value="" /><img style="width: 100px; height: 100px" src="image?id=${dto.itemid}"></td>
						<td><div align="center"><c:out value="${dto.name}" /></div></td>
						<td><input type="text" name="identNum" value=""></td>
						<td></td>
					</tr>
				</c:forEach>
			</table>
			<c:if test="${not empty errorMsg}"><font color="FF0000"><c:out value="${errorMsg}"/></font><br></c:if>
			<br><input type="submit" value="貸出">
			<% session.removeAttribute("errorMsg"); %>
		</form>
	</c:if>
	<c:if test="${rental_dto.status != '受付' }">


		<table border="1">
			<tr>
				<th>商品番号</th>
				<th>新旧区分</th>
				<th>カテゴリ</th>
				<th>ジャンル</th>
				<th>商品画像</th>
				<th>タイトル</th>
				<th>個体識別番号</th>
				<th>返却処理</th>
			</tr>
			<c:forEach var="dto" items="${item_dto}">
			<form action="/MMRental_Admin/control" method="POST">
				<input type="hidden" name="action" value="returnUpdate">
				<input type="hidden" name="rentNum" value="${rental_dto.rentalNum}">
				<input type="hidden" name="status" value="${rental_dto.status}">
				<input type="hidden" name="itemid" value="${dto.itemid}">
				<input type="hidden" name="identNum" value="${dto.identificationNum}">
				<tr>
					<td><div align="right"><c:out value="${dto.itemid}" /></div></td>
					<td><div align="center"><c:out value="${dto.newAndOld}" /></div></td>
					<td><div align="center"><c:out value="${dto.category}" /></div></td>
					<td><div align="center"><c:out value="${dto.genre}" /></div></td>
					<td><c:out value="" /><img style="width: 100px; height: 100px" src="image?id=${dto.itemid}"></td>
					<td><div align="center"><c:out value="${dto.name}" /></div></td>
					<td><div align="right"><c:out value='${dto.identificationNum}' /></div></td>
					<td><c:if test="${dto.returnFlg == false}">
					<div align="center"><c:choose>
						<c:when test="${rental_dto.status == '滞納'}">
							<input type="submit" value="返却">
						</c:when>
						<c:when test="${rental_dto.status == '貸出'}">
							<input type="submit" value="返却">
						</c:when>
						<c:otherwise>
						</c:otherwise>
					</c:choose></div>
					</c:if></td>
				</tr>
			</form>
			</c:forEach>
		</table>

	</c:if>
</body>
</html>