<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>パスワード変更登録</title>
</head>
<body>
	<form action="/MMRental_Admin/control" method="POST">
		<h2>パスワード変更登録</h2>
		<hr>
		<div align="center">
			<table style="border-collapse: collapse">
				<tr>
					<td>パスワードが初期値のままです。</td>
				</tr>
				<tr>
					<td>変更してください。</td>
				</tr>
				<tr>
					<td><font color="FF0000">${errorMsg}</font></td>
				</tr>
			</table>
			<table border="1" style="border-collapse: collapse">
				<tr>
					<th bgcolor="#FFBDFF">パスワード：</th>
					<td><input type="password" name="pass">
					<td><br>
				</tr>
				<tr>
					<th bgcolor="#FFBDFF">パスワード確認：</th>
					<td><input type="password" name="check">
					<td><br>
				</tr>
			</table>
			<br>
			<input type="hidden" name="action" value="passUpdate"> <input
				type="submit" value="パスワード変更">
		</div>
	</form>
</body>
</html>