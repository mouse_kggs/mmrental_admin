<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@
	page import="dto.GenreDTO, java.util.ArrayList, dao.GenreDAO"
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script type="text/javascript" src="js/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="js/categorySelecter.js"></script>
<title>商品マスタメンテナンス</title>
</head>
<body>
<jsp:include page="header_common.jsp" flush="true" /><br>
<%
	request.setAttribute("categoryList", GenreDAO.selectCategoryAllStatic());
	request.setAttribute("genreList", GenreDAO.selectGenreAllStatic());
%>

<h2>商品マスタメンテナンス</h2><hr>
<form action="/MMRental_Admin/control" method="POST">
	商品追加はこちら<br>
	<input type="hidden" name="action" value="itemAddScene">
	<input type="submit" value="商品追加">
</form>
<c:if test="${not empty msg}"><h3 style="color:red">${msg}</h3></c:if>
<c:if test="${empty msg}">
<form action="/MMRental_Admin/control" method="POST" name="test">
	<input type="hidden" name="action" value="itemSearch">
	<table>
		<tr>
			<th><font color="FFFFFF">　　　　</font></th>
			<th><font color="FFFFFF">　　　　</font></th>
			<th>カテゴリ</th>
			<th>ジャンル</th>
			<th>タイトル</th>
			<th><font color="FFFFFF">　　</font></th>
		</tr>
		<tr>
			<th><font color="FFFFFF">　　　　</font></th>
			<th><font color="FFFFFF">　　　　</font></th>
			<th>
			<select id="categorySelectBox" class="categoryBox" name="category" onChange="categoryChanged()">
				<option value="0" <c:if test="${searchCategory == 0}"><c:out value="selected"/></c:if>>カテゴリ</option>
				<c:forEach var="category" items="${categoryList}">
					<option value='<c:out value="${category.category_id}"/>' <c:if test="${searchCategory == category.category_id}"><c:out value="selected"/></c:if>><c:out value="${category.category_name}"/></option>
				</c:forEach>
			</select>
			</th>
			<th>
				<select id="genreSelectBox" class="genreBox" name="genre">
					<option value="0" >ジャンル</option>
					<c:forEach var="genre" items="${genreList}">
						<option class='<c:out value="${genre.category_id}"/>' value='<c:out value="${genre.genre_id}"/>' <c:if test="${searchGenre == genre.genre_id}"><c:out value="selected"/></c:if>><c:out value="${genre.genre_name}"/></option>
					</c:forEach>
				</select>
			</th>
			<th><input type="text" name="title" value="${searchString}"></th>
			<th><input type="submit" value="検索"></th>
		</tr>
	</table>
	</form>

	<table border="1" style="border-collapse: collapse">
		<tr>
			<th bgcolor="#FFBDFF">商品番号</th>
			<th bgcolor="#FFBDFF">新旧区分</th>
			<th bgcolor="#FFBDFF">カテゴリ</th>
			<th bgcolor="#FFBDFF">商品画像</th>
			<th bgcolor="#FFBDFF">タイトル</th>
			<th bgcolor="#FFBDFF">アーティスト/監督</th>
			<th bgcolor="#FFBDFF">値段</th>
			<th bgcolor="#FFBDFF">詳細</th>
		</tr>
		<c:forEach var="item" items="${item_dto}" >
		<form action="/MMRental_Admin/control" method="POST">
			<input type="hidden" name="action" value="itemDetail">
			<input type="hidden" name="id" value="${item.id}">
			<tr>
				<td><div align="right"><c:out value="${item.id}"/></div></td>
				<td><div align="center"><c:out value="${item.newAndOld}"/></div></td>
				<td><div align="center"><c:out value="${item.category}"/></div></td>
				<td><c:out value=""/><img style="width: 100px; height: 100px" src="image?id=${ item.id }"></td>
				<td><c:out value="${item.name}"/></td>
				<td><c:out value="${item.artist}"/></td>
				<td><div align="right"><c:out value="${item.price}"/></div></td>
				<td><center><input type="submit" value="詳細"></center></td>
			</tr>
		</form>
		</c:forEach>
	</table>
<div align="center"><font size="4">
	<c:if test="${(page-1) > 0}"><a href='paging?page=<c:out value="${page-1}"/>'>&lt; 前のページ</a></c:if>

	<c:if test="${(page-3) > 0}"><a href='paging?page=<c:out value="${page-3}"/>'><c:out value="${page-3}"/></a></c:if>
	<c:if test="${(page-2) > 0}"><a href='paging?page=<c:out value="${page-2}"/>'><c:out value="${page-2}"/></a></c:if>
	<c:if test="${(page-1) > 0}"><a href='paging?page=<c:out value="${page-1}"/>'><c:out value="${page-1}"/></a></c:if>
	<b><c:out value="${page}"/></b>
	<c:if test="${(page+1) <= pageMax}"><a href='paging?page=<c:out value="${page+1}"/>'><c:out value="${page+1}"/></a></c:if>
	<c:if test="${(page+2) <= pageMax}"><a href='paging?page=<c:out value="${page+2}"/>'><c:out value="${page+2}"/></a></c:if>
	<c:if test="${(page+3) <= pageMax}"><a href='paging?page=<c:out value="${page+3}"/>'><c:out value="${page+3}"/></a></c:if>

	<c:if test="${(page+1) <= pageMax}"><a href='paging?page=<c:out value="${page+1}"/>'>次のページ &gt;</a></c:if>
</font></div>
<br>
</c:if>
</body>
</html>