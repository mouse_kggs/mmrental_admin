package action;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import dao.DAOException;
import dao.StaffDAO;
import dto.StaffDTO;

public class StaffMenuAction implements Action {

	@Override
	public boolean check(HttpServletRequest req) {
		return true;
	}

	@Override
	public String execute(HttpServletRequest req) throws DAOException {
		HttpSession session = req.getSession(false);
		if (session != null) {

			StaffDAO dao = new StaffDAO();
			ArrayList<StaffDTO> list = dao.selectUndeletedStaff();

			if (!(list.isEmpty())) {
				session.setAttribute("staff_dto", list);
				return "/WEB-INF/jsp/StaffMenu.jsp";
			}
		}
		return "/WEB-INF/jsp/error.jsp";
	}

}
