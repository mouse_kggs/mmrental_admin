package action;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;

import dao.DAOException;
import dao.ItemDAO;
import dto.ItemDTO;

public class ItemAddAcion implements Action {
	private int newAndOld_id = 0;
	private int category_id = 0;
	private int genre_id = 0;
	private InputStream image;
	private String title = null;
	private String artist = null;
	private int price = 0;
	private int maxIdentNum = 0;
	private boolean recommend = false;
	private String remarks = null;

	private String category = null;
	private String genre = null;
	private String sPrice = null;
	private String sMaxIdentNum = null;

	private boolean category_flg = false;
	private boolean genre_flg = false;
	private boolean title_flg = false;
	private boolean artist_flg = false;
	private boolean sPrice_flg = false;
	private boolean sMaxIdentNum_flg = false;

	@Override
	public boolean check(HttpServletRequest req) {
		String newAndOld = req.getParameter("newAndOld");
		category = req.getParameter("category");
		genre = req.getParameter("genre");
		title = req.getParameter("title");
		artist = req.getParameter("artist");
		sPrice = req.getParameter("price");
		sMaxIdentNum = req.getParameter("maxIdentNum");
		String sRecommend = req.getParameter("recommend");
		remarks = req.getParameter("remarks");

		nullCheck();

		try {
			newAndOld_id = Integer.parseInt(newAndOld);
			category_id = Integer.parseInt(category);
			genre_id = Integer.parseInt(genre);
			price = Integer.parseInt(sPrice);
			maxIdentNum = Integer.parseInt(sMaxIdentNum);

			if (sRecommend == null) {
				recommend = false;
			} else {
				switch (sRecommend) {
				case "":
					recommend = false;
					break;
				case "0":
					recommend = false;
					break;
				case "1":
					recommend = true;
					break;
				default:
					return false;
				}
			}
		} catch (NumberFormatException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	@Override
	public String execute(HttpServletRequest req) throws DAOException {
		HttpSession session = req.getSession(false);

		String msg = null;

		if (session != null) {
			try {
				for (Part p : req.getParts()) {
					if ("file".equals(p.getName())) {
						image = p.getInputStream();
					}
				}
			} catch (ServletException | IOException e) {
				image = null;
				e.printStackTrace();
			}

			ItemDAO dao = new ItemDAO();
			ItemDTO dto = null;
			int result = 0;
			if (!(castImage())) {
				dto = new ItemDTO(newAndOld_id, category_id, genre_id, title, artist, price, maxIdentNum, recommend,
						remarks);

				msg = flgCheck();
				if (msg != null) {
					req.setAttribute("errorMsg", msg);
					session.setAttribute("item", dto);
					flgReset();
					return "/WEB-INF/jsp/ItemDetail.jsp";
				}

				result = dao.add(dto);
			} else {
				dto = new ItemDTO(newAndOld_id, category_id, genre_id, image, title, artist, price, maxIdentNum,
						recommend, remarks);

				msg = flgCheck();
				if (msg != null) {
					req.setAttribute("errorMsg", msg);
					session.setAttribute("item", dto);
					flgReset();
					return "/WEB-INF/jsp/ItemDetail.jsp";
				}

				result = dao.add(dto);
			}

			if (result == 1) {
				ItemMenuAction action = new ItemMenuAction();
				String nextPage = action.execute(req);
				return nextPage;
			}
		}
		return "/WEB-INF/jsp/DBerror.jsp";
	}

	private boolean castImage() {
		try {
			BufferedImage test = ImageIO.read(image);
			BufferedImage tmp = new BufferedImage(test.getWidth(), test.getHeight(), BufferedImage.TYPE_INT_RGB);
			Graphics2D off = tmp.createGraphics();
			off.drawImage(test, 0, 0, Color.WHITE, null);

			// 一度byte配列へ変換
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			ImageIO.write(tmp, "jpg", baos);
			baos.flush();
			byte[] imageInByte = baos.toByteArray();
			baos.close();

			// byte配列をInputStreamに変換
			image = new ByteArrayInputStream(imageInByte);

			return true;
		} catch (IOException | NullPointerException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
			return false;
		}
	}

	private void nullCheck() {
		if (category == null || category.equals("") || category.equals("0")) {
			category_flg = true;
			category = "0";
		}
		if (genre == null || genre.equals("") || genre.equals("0")) {
			genre_flg = true;
			genre = "0";
		}
		if (title == null || title.equals("") || title.equals("0")) {
			title_flg = true;
		}
		if (sPrice == null || sPrice.equals("") || sPrice.equals("0")) {
			sPrice_flg = true;
			sPrice = "0";
		}
		if (artist == null || artist.equals("") || artist.equals("0")) {
			artist_flg = true;
		}
		if (sMaxIdentNum == null || sMaxIdentNum.equals("") || sMaxIdentNum.equals("0")) {
			sMaxIdentNum_flg = true;
			sMaxIdentNum = "0";
		}
	}

	private String flgCheck() {
		String msg = null;
		if (category_flg) {
			msg = "カテゴリが選択されていません。";
			return msg;
		} else if (genre_flg) {
			msg = "ジャンルが選択されていません。";
			return msg;
		} else if (title_flg) {
			msg = "タイトルが入力されていません。";
			return msg;
		} else if (sPrice_flg) {
			if (sPrice.equals("0")) {
				msg = "値段が入力されていません。1以上の数値を入力してください。";
			} else {
				msg = "値段が入力されていません。";
			}
			return msg;
		} else if (artist_flg) {
			msg = "アーティスト/監督が入力されていません。";
			return msg;
		} else if (sMaxIdentNum_flg) {
			if (sMaxIdentNum.equals("0")) {
				msg = "採番番号が入力されていません。1以上の数値を入力してください。";
			} else {
				msg = "採番番号が入力されていません。";
			}
			return msg;
		}
		return msg;
	}

	private void flgReset() {
		category_flg = false;
		genre_flg = false;
		title_flg = false;
		artist_flg = false;
		sPrice_flg = false;
		sMaxIdentNum_flg = false;
	}
}
