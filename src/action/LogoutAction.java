package action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import dao.DAOException;

public class LogoutAction implements Action {

	@Override
	public boolean check(HttpServletRequest req) {
		return true;
	}

	@Override
	public String execute(HttpServletRequest req) throws DAOException {
		HttpSession session = req.getSession(false);
		if (session != null) {
			session.invalidate();
		}
		return "/WEB-INF/jsp/Login.jsp";
	}

}
