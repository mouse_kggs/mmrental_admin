package action;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import dao.DAOException;
import dao.StaffDAO;
import dto.StaffDTO;

public class PassUpadateAction implements Action{
	String pass = null;
	String check = null;
	public boolean check(HttpServletRequest req){
		pass = req.getParameter("pass");
		check = req.getParameter("check");

		return true;
	}

	public String execute(HttpServletRequest req) throws DAOException  {
		String msg = null;
		String nextPage = null;

		if((pass.equals("")) && (check.equals(""))){
			msg = "※値を入れてください。";
			req.setAttribute("errorMsg", msg);
			return "/WEB-INF/jsp/Password.jsp";
		}

		if(!pass.equals("default") && pass.equals(check)){
			nextPage = exe(req);
			return nextPage;
		}else if(pass.equals("default")){
			msg = "※初期値では登録できません";
			req.setAttribute("errorMsg", msg);
			return "/WEB-INF/jsp/Password.jsp";
		}else{
			msg = "※パスワードが間違っています。";
			req.setAttribute("errorMsg", msg);
			return "/WEB-INF/jsp/Password.jsp";
		}
	}

	public String exe(HttpServletRequest req) throws DAOException{
		String nextPage = null;
		StaffDAO dao = new StaffDAO();
		HttpSession session = req.getSession(false);

		if(session != null){
			StaffDTO staff = (StaffDTO) session.getAttribute("login_dto");
			String id = staff.getId();
			int result = dao.passUpdate(id, pass);

			StaffDAO staffdao = new StaffDAO();
			ArrayList<StaffDTO> staffList = staffdao.all();


			for(int i=0;i<staffList.size();i++){
				StaffDTO checkid = staffList.get(i);
				String test= checkid.getId();
				if(test.equals(id)){
					staff.setManager_flg(checkid.getManager_flg());
				}
			}

			session.setAttribute("login_dto",staff);
			session.setAttribute("staff_dto", staffList);

			if(result == 1){
				RentalMenuAction rentalObj = new RentalMenuAction();
				nextPage = rentalObj.execute(req);

				return nextPage;
			}
		}
		return null;
	}
}
