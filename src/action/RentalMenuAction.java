package action;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import dao.DAOException;
import dao.RentalDAO;
import dto.RentalDTO;

public class RentalMenuAction implements Action{

	@Override
	public boolean check(HttpServletRequest req) {
		return true;
	}

	@Override
	public String execute(HttpServletRequest req) throws DAOException {
		//req.removeAttribute("history");
		RentalDAO dao = new RentalDAO();
		ArrayList<RentalDTO> list = dao.all10();

		// データ件数の取得
		int dataNum = dao.searchNum();
		System.out.println(dataNum);
		req.getSession().setAttribute("pageMax", (int)((dataNum + 9) / 10) );
		req.setAttribute("page", 1);

		HttpSession session = req.getSession(false);
		if(session == null){
			return "/WEB-INF/jsp/error.jsp";
		}
		session.setAttribute("rental_dto", list);


		return "/WEB-INF/jsp/RentalMenu.jsp";
	}

}
