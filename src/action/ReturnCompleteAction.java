package action;

import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import dao.DAOException;
import dao.RentalDAO;
import dao.RentalDetailDAO;
import dto.RentalDetailDTO;
import dto.StaffDTO;

public class ReturnCompleteAction implements Action {
	private int rentNum = 0;
	private int itemid = 0;
	private int identificationNum = 0;
	private String returnid = null;
	private String status = null;

	@Override
	public boolean check(HttpServletRequest req) {
		String sRentNum = req.getParameter("rentNum");
		String sitemid = req.getParameter("itemid");
		String sidentNum = req.getParameter("identNum");
		status = req.getParameter("status");

		try {
			rentNum = Integer.parseInt(sRentNum);
			itemid = Integer.parseInt(sitemid);
			identificationNum = Integer.parseInt(sidentNum);

			switch (status) {
			case "滞納":
				status = "4"; // 返却に変更
				break;
			case "貸出":
				status = "4"; // 返却に変更
				break;
			default:
				return false;
			}
			return true;
		} catch (NumberFormatException e) {
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public String execute(HttpServletRequest req) throws DAOException {
		HttpSession session = req.getSession(false);
		if (session == null) {
			return "/WEB-INF/jsp/error.jsp";
		}

		RentalDAO dao = new RentalDAO();
		RentalDetailDAO detail = new RentalDetailDAO();
		int rentalResult = dao.returnItem(rentNum, itemid, identificationNum);
		if (rentalResult == 1) {
			ArrayList<RentalDetailDTO> list = detail.returnFlgCheck(rentNum);
			int statusResult = 0;
			if (checkFlg(list)) {
				StaffDTO dto = (StaffDTO)session.getAttribute("login_dto");
				returnid = dto.getId();
				statusResult = dao.returnUpdate(status, rentNum, returnid);
			}

			if (rentalResult == 1 && statusResult == 1) {
				return new RentalMenuAction().execute(req);
			} else if (rentalResult == 1) {
				return new RentalDetailAction().execute(req);
			}
		}
		return "/WEB-INF/jsp/error.jsp";
	}

	public Date getDate() {
		Date date = null;
		Calendar cal = Calendar.getInstance();
		String returnDate = cal.getTime().toString();
		date = (Date) castDate(returnDate);
		return date;
	}

	// Date型変換
	public java.util.Date castDate(String date) {
		Date formatDate = null;
		try {
			if (!(date.isEmpty())) {
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
				formatDate = new Date(sdf.parse(date).getTime());
			}
			return formatDate;
		} catch (ParseException e) {
			e.printStackTrace();
			return null;
		}
	}

	// 返却フラグのチェック
	public boolean checkFlg(ArrayList<RentalDetailDTO> list) {
		ArrayList<Boolean> flg = new ArrayList<Boolean>();
		// レンタル詳細の返却フラグを取得
		for (int i = 0; i < list.size(); i++) {
			RentalDetailDTO dto = list.get(i);
			flg.add(dto.getReturnFlg());
		}

		// 返却フラグがすべてtrueかチェック
		boolean flag = true;
		for (Boolean check : flg) {
			flag &= check;
			if (!check) {
				flag = false;
				break;
			}
		}

		// 返却フラグがfalseならステータスは更新しない
		if (!flag) {
			return false;
		}
		return true;
	}
}
