package action;

import javax.servlet.http.HttpServletRequest;

import dao.DAOException;

public class ItemEditCancel implements Action {
	String nextPage = null;
	@Override
	public boolean check(HttpServletRequest req) {
		return true;
	}

	@Override
	public String execute(HttpServletRequest req) throws DAOException {
		ItemMenuAction action = new ItemMenuAction();
		nextPage = action.execute(req);
		return nextPage;
	}

}
