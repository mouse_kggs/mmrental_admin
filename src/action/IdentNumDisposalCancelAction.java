package action;

import javax.servlet.http.HttpServletRequest;

import dao.DAOException;
import dao.ItemDAO;

public class IdentNumDisposalCancelAction implements Action {
	private int id;
	private int disNum[] = null;
	private boolean disNullCheck = false;

	@Override
	public boolean check(HttpServletRequest req) {
		String sid = req.getParameter("id");
		String num[] = req.getParameterValues("disNum");

		try {
			id = Integer.parseInt(sid);
			if (num != null) {
				disNum = new int[num.length];
				for (int i = 0; i < num.length; i++) {
					disNum[i] = Integer.parseInt(num[i]);
				}
				return true;
			}
			disNullCheck = true;
			return true;
		} catch (NumberFormatException e) {
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public String execute(HttpServletRequest req) throws DAOException {

		if (!(disNullCheck)) {
			ItemDAO dao = new ItemDAO();
			for (int i = 0; i < disNum.length; i++) {
				int result = dao.disNumCancel(id, disNum[i]);
				if (result != 1) {
					return "/WEB-INF/jsp/DBerror.jsp";
				}
			}
			return "/WEB-INF/jsp/ItemMenu.jsp";
		}

		String msg = "チェックをしてください";
		req.setAttribute("cancel", msg);
		return "/WEB-INF/jsp/ItemDetail.jsp";
	}

}
