package action;

import javax.servlet.http.HttpServletRequest;

import dao.DAOException;

public interface Action{
	 boolean check(HttpServletRequest req);
	 String execute(HttpServletRequest req) throws DAOException;
}
