package action;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import dao.DAOException;
import dao.DisposalidentificationNumberDAO;
import dao.RentalDAO;
import dto.RentalDetailDTO;
import dto.StaffDTO;

public class RentalCompleteAction implements Action {
	private int rentNum = 0;
	private ArrayList<Integer> idList = null;
	private ArrayList<Integer> numList = null;
	private String status = null;
	private String msg = null;
	private boolean error_flg = false;

	@Override
	public boolean check(HttpServletRequest req) {
		String sRentNum = req.getParameter("rentNum");
		String[] sitemid = req.getParameterValues("itemid");
		String[] sidentNum = req.getParameterValues("identNum");
		status = req.getParameter("status");

		switch (status) {
		case "受付":
			status = "3";
			break;
		default:
			return false;
		}

		try {
			rentNum = Integer.parseInt(sRentNum);
			if (sitemid != null) {
				idList = new ArrayList<Integer>();
				for (int i = 0; i < sitemid.length; i++) {
					int test = Integer.parseInt(sitemid[i]);
					idList.add(test);
				}
			}
			if (sidentNum != null) {
				numList = new ArrayList<>();
				for (int i = 0; i < sidentNum.length; i++) {
					numList.add(Integer.parseInt(sidentNum[i]));
				}
			}
		} catch (NumberFormatException e) {
			e.printStackTrace();
			error_flg = true;
			try {
				execute(req);
			} catch (DAOException e1) {
				// TODO 自動生成された catch ブロック
				e1.printStackTrace();
			}
		}
		return true;
	}

	@SuppressWarnings("unchecked")
	@Override
	public String execute(HttpServletRequest req) throws DAOException {
		HttpSession session = req.getSession(false);
		if(error_flg){
			msg = "個体識別番号は数値で入力してください。";
			session.setAttribute("errorMsg", msg);
			return "/WEB-INF/jsp/RentalDetail.jsp";
		}
		DisposalidentificationNumberDAO disNum = new DisposalidentificationNumberDAO();
		ArrayList<RentalDetailDTO> rental = new ArrayList<>();

		ArrayList<Boolean> nullCheck = new ArrayList<>();

		RentalDetailDTO dto = null;
		RentalDAO rent = new RentalDAO();
		int rentalResult = 0;

		if (session != null) {
			int itemid = 0;
			ArrayList<RentalDetailDTO> list = (ArrayList<RentalDetailDTO>) session.getAttribute("item_dto");
			for (int i = 0; i < list.size(); i++) {
				dto = list.get(i);
				itemid = dto.getItemid();
				if (itemid == idList.get(i)) {
					dto.setIdentificationNum(numList.get(i));
					rental.add(dto);
				}
				nullCheck.add(disNum.disNumCheck(dto.getItemid(), dto.getIdentificationNum()));
			}

			boolean flag = true;
			for (Boolean check : nullCheck) {
				if(!(flag &= check)){
					flag = false;
					break;
				}
			}

			if(flag) {
				for(int i=0;i<rental.size();i++){
					rentalResult = rent.rental(rentNum, idList.get(i), numList.get(i));
				}
			}

			if (rentalResult == 1) {
				StaffDTO staff = (StaffDTO) session.getAttribute("login_dto");
				int statusResult = rent.rentalUpdate(status, rentNum, staff.getId());
				if (rentalResult == 1 && statusResult == 1) {
					return new RentalMenuAction().execute(req);
				}
			}
		}

		msg="存在しないか、貸出中の個体識別番号です。";
		session.setAttribute("errorMsg", msg);
		return "/WEB-INF/jsp/RentalDetail.jsp";
	}

}
