package action;

import java.sql.Date;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import dao.DAOException;
import dao.RentalDAO;
import dto.RentalDTO;
import dto.RentalHistoryDTO;

public class RentalSearchAction implements Action {
	private String rentalid = null;
	private String returnid = null;
	private String status = null;

	private String sOrderDate = null;
	private String sReturnDate = null;
	private String sReturnSchedule = null;

	boolean castError = false;

	@Override
	public boolean check(HttpServletRequest req) {
		sOrderDate = req.getParameter("orderDate");
		sReturnDate = req.getParameter("returnDate");
		sReturnSchedule = req.getParameter("returnSchedule");
		rentalid = req.getParameter("rentalid");
		returnid = req.getParameter("returnid");
		status = req.getParameter("status");

		if(rentalid.equals("0")){
			rentalid = "";
		}

		if(returnid.equals("0")){
			returnid = "";
		}

		try {
			int intStatus = Integer.parseInt(status);
			switch (intStatus) {
			case 0:
				status = "";
				break;
			case 1:
				status = "受付";
				break;
			case 2:
				status = "延滞";
				break;
			case 3:
				status = "貸出";
				break;
			case 4:
				status = "返却";
				break;
			}
		} catch (NumberFormatException e) {
			e.printStackTrace();
		}

		return true;
	}

	@Override
	public String execute(HttpServletRequest req) throws DAOException {
		RentalHistoryDTO histroy = null;
		ArrayList<RentalDTO> list = null;

		RentalDAO dao = new RentalDAO();
		RentalDTO dto = new RentalDTO(sOrderDate, sReturnSchedule, sReturnDate, rentalid, returnid, status);
		// 検索条件の保存
		req.getSession().setAttribute("searchCondition", dto);

		histroy = new RentalHistoryDTO(sOrderDate, sReturnDate, sReturnSchedule, rentalid, returnid, status);
		req.getSession().setAttribute("history", histroy);

		//日付に文字列が入っていないかチェック
//		if(castCheck()){
//			String msg = "検索条件に該当するデータがありません。";
//			req.setAttribute("errorMsg", msg);
//			return "/WEB-INF/jsp/RentalMenu.jsp";
//		}

		// データ件数の取得
		int dataNum = dao.searchNum(dto);
		req.getSession().setAttribute("pageMax", (int)((dataNum + 9) / 10) );

		// データ検索
		list = dao.search(dto,1);
		req.setAttribute("page", 1);

		if (!list.isEmpty()) {
			HttpSession session = req.getSession();
			session.setAttribute("rental_dto", list);
			return "/WEB-INF/jsp/RentalMenu.jsp";
		}

		String msg = "検索条件に該当するデータがありません。";
		req.setAttribute("errorMsg", msg);
		return "/WEB-INF/jsp/RentalMenu.jsp";
	}


	@SuppressWarnings("unused")
	public boolean castCheck(){
//		String regex2 = "[0-9][0-9a-fA-F]";
//	    Pattern p2 = Pattern.compile(regex2);
		try{
			if(!(sOrderDate.isEmpty())){
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		        // Timestamp型変換
		        Timestamp formatTimestamp = (Timestamp) sdf.parse(sOrderDate);
			}
			if(!(sReturnSchedule.isEmpty())){
				SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		        // Date型変換
		        Date formatDate = (Date) sdf.parse(sReturnSchedule);
			}
			if(!(sReturnDate.isEmpty())){
				SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		        // Date型変換
				Date formatDate = (Date) sdf.parse(sReturnDate);
			}

			return castError;
		}catch(ParseException e){
			castError = true;
			return castError;
		}
	}
}
