package action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import dao.DAOException;
import dao.ItemDAO;
import dto.ItemDTO;

public class IdentNumDisposalAction implements Action {
	private int id;
	private int disNum;

	@Override
	public boolean check(HttpServletRequest req) {
		String sid = req.getParameter("id");
		String num = req.getParameter("disNum");

		try {
			id = Integer.parseInt(sid);
			disNum = Integer.parseInt(num);
			return true;
		} catch (NumberFormatException e) {
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public String execute(HttpServletRequest req) throws DAOException {
		ItemDAO dao = new ItemDAO();

		HttpSession session = req.getSession(false);
		ItemDTO dto = (ItemDTO)session.getAttribute("item");
		int max = dto.getMaxIdentificationNum();

		int result = 0;
		if(disNum > 0 && max >= disNum){
			result = dao.disNum(id, disNum);
		}
		if(result == 1){
			return "/WEB-INF/jsp/ItemMenu.jsp";
		}

		String msg = "存在しない個体識別番号です。";
		req.setAttribute("error", msg);
		return "/WEB-INF/jsp/ItemDetail.jsp";
	}

}
