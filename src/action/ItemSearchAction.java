package action;

import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import dao.DAOException;
import dao.ItemDAO;
import dto.ItemDTO;

public class ItemSearchAction implements Action {
	String category = null;
	String genre = null;
	String name = null;
	int page = 1;

	@Override
	public boolean check(HttpServletRequest req) {
		//req.setCharacterEncoding("UTF-8");
		category = req.getParameter("category");
		genre = req.getParameter("genre");
		name = req.getParameter("title");
		String pageStr = req.getParameter("page");

		if (category == null || category.equals("0")) {
			category = "0";
		}
		if (genre == null || genre.equals("0")) {
			genre = "0";
		}
		if(name == null){
			name = "";
		}
		if(pageStr == null || Integer.parseInt(pageStr) < 1){
			pageStr = "1";
			page = 1;
		}else{
			page = Integer.parseInt(pageStr);
		}

		return true;
	}

	@Override
	public String execute(HttpServletRequest req) throws DAOException {
		HttpSession session = req.getSession(false);
		if (session == null) {
			return "/WEB-INF/jsp/error.jsp";
		}
		ArrayList<ItemDTO> list = null;
		ItemDAO dao = new ItemDAO();
		try {
			// データ件数を取得する
			int dataNum = dao.getItemNum(category, genre, name);
			// セッションに結果を保存
			session.setAttribute("pageMax", (int)((dataNum + 9) / 10) );

			// 検索を行う
			list = dao.searchPage(category, genre, name,page);
			System.out.println("ok" + list.size());
		} catch (SQLException e) {
			e.printStackTrace();
			return "/WEB-INF/jsp/DBerror.jsp";
		}

		// 検索条件の保存
		session.setAttribute("searchCategory", category);
		session.setAttribute("searchGenre", genre);
		session.setAttribute("searchString", name);
		req.setAttribute("page", page);

		if (list.isEmpty()) {
			req.setAttribute("msg", "指定の条件では見つかりませんでした");
		}
		session.setAttribute("item_dto", list);
		return "/WEB-INF/jsp/ItemMenu.jsp";
	}

}
