package action;

import java.io.UnsupportedEncodingException;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;

import dto.StaffDTO;

public class StaffCheck {

	public static String checkStaffData(HttpServletRequest req, StaffDTO dto) throws UnsupportedEncodingException{
		// エラーメッセージ
		StringBuffer msg = new StringBuffer("");

		// 各項目に入力値チェックを行い、チェックが通った項目はEmpDTOオブジェクトに追加する
		String data;

		req.setCharacterEncoding("UTF-8");

		// 氏名
		data = req.getParameter("name");
		if(data != null && data.length() <= 30 && data.length() > 0){
			// OK
			dto.setName(data);
		}else if(data.length() > 30){
			// 30文字以内でない
			msg.append("氏名は30文字以内で入力してください<br>");
		}else{
			// 0文字 もしくは null
			msg.append("氏名を30文字以内で入力してください<br>");
		}

		// ID
		data = req.getParameter("newId");
		if(data != null && Pattern.compile("\\d+").matcher(data).matches() && data.length() <= 30 && data.length() > 0){
			// OK
			dto.setId(data);
		}else if(data.length() > 30){
			// 30文字以内でない
			msg.append("IDは30字以内で入力してください<br>");
		}else{
			// 0文字 もしくは null
			msg.append("IDを30字以内の数字を入力してください<br>");
		}

		// 管理者フラグ
		data = req.getParameter("flag");
		if(data != null && "true".equals(data)){
			// OK
			dto.setManager_flg(true);
		}else{
			dto.setManager_flg(false);
		}

		// パスワード（デフォルト）と削除フラグを設定
		dto.setPass("default");
		dto.setDel_flg(false);

		// エラーメッセージを返却する（エラーが無ければ空文字で返る）
		return msg.toString();
	}
}
