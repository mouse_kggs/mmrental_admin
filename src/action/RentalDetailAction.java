package action;

import java.sql.Date;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import dao.DAOException;
import dao.ItemDAO;
import dto.RentalDTO;
import dto.RentalDetailDTO;

public class RentalDetailAction implements Action {
	private int id = 0;
	private Timestamp orderDate = null;
	private Date returnSchedule = null;
	private Date returnDate = null;
	private int stock = 0;
	private String rentalid = null;
	private String returnid = null;
	private String status = null;

	@Override
	public boolean check(HttpServletRequest req) {
		String sid = req.getParameter("id");
		String sStock = req.getParameter("stock");
		rentalid = req.getParameter("rentalid");
		returnid = req.getParameter("returnid");
		status = req.getParameter("status");

		//型キャスト
		orderDate = (Timestamp) castTimestamp(req.getParameter("orderDate"));
		returnSchedule = (Date) castDate(req.getParameter("returnSchedule"));
		returnDate = (Date) castDate(req.getParameter("returnDate"));

		try {
			id = Integer.parseInt(sid);
			stock = Integer.parseInt(sStock);
		} catch (NumberFormatException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	@Override
	public String execute(HttpServletRequest req) throws DAOException {
		HttpSession session = req.getSession(false);
		RentalDTO rental = new RentalDTO(id, orderDate, returnSchedule, returnDate, stock, rentalid, returnid, status);
		ItemDAO dao = new ItemDAO();

		if(rental.getOrderDate() == null){
			rental = (RentalDTO)session.getAttribute("rental_dto");
			id = (int)session.getAttribute("rentalId");
		}

		ArrayList<RentalDetailDTO> list = dao.rentalDetail(id);
		
		if (session != null && rental != null) {
			session.setAttribute("rental_dto", rental);
			session.setAttribute("item_dto", list);
			session.setAttribute("rentalId",id);
			return "/WEB-INF/jsp/RentalDetail.jsp";
		}

		return "/WEB-INF/jsp/error.jsp";
	}

	// Timestamp型変換
	public java.util.Date castTimestamp(String timestamp) {
		Timestamp formatTimestamp = null;
		try {
			if (!(timestamp.isEmpty())) {
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

				formatTimestamp = new Timestamp(sdf.parse(timestamp).getTime());
			}
			return formatTimestamp;
		} catch (ParseException e) {
			return null;
		}
	}

	 // Date型変換
	public java.util.Date castDate(String date){
		Date formatDate = null;
		try{
			if(!(date.isEmpty())){
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

		        formatDate = new Date(sdf.parse(date).getTime());
			}
			return formatDate;
		} catch (ParseException e) {
			e.printStackTrace();
			return null;
		}
	}
}
