package action;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import dao.DAOException;
import dao.StaffDAO;
import dto.StaffDTO;

public class LoginAction implements Action{
	@SuppressWarnings("unused")
	private int id = 0;
	private String pass = null;
	private String def = "default";
	private String sid = null;

	public boolean check(HttpServletRequest req){
		sid = req.getParameter("id");
		pass = req.getParameter("pass");

		return true;
	}

	public String execute(HttpServletRequest req) throws DAOException{
		StaffDAO staff = new StaffDAO();
		HttpSession session = req.getSession(true);
		StaffDTO dto = new StaffDTO();

		if(staff.search(sid, pass,dto) && !staff.search(sid, def,dto)){
			dto.setId(sid);
			session.setAttribute("login_dto", dto);

			StaffDAO staffdao = new StaffDAO();
			ArrayList<StaffDTO> staffList = staffdao.all();
			session.setAttribute("staff_dto", staffList);

			String nextPage = null;
			RentalMenuAction rentalObj = new RentalMenuAction();
			nextPage = rentalObj.execute(req);

			return nextPage;
		}else if(!staff.search(sid, pass,dto)){
			String msg="※ログインIDかパスワードが間違っています。";
			req.setAttribute("errorMsg", msg);
			return "/WEB-INF/jsp/Login.jsp";
		}else{
			dto = new StaffDTO(sid);
			session.setAttribute("login_dto", dto);
			return "/WEB-INF/jsp/Password.jsp";
		}
	}
}
