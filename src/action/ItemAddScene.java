package action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import dao.DAOException;

public class ItemAddScene implements Action {

	@Override
	public boolean check(HttpServletRequest req) {
		return true;
	}

	@Override
	public String execute(HttpServletRequest req) throws DAOException {
		HttpSession session = req.getSession(false);
		if(session != null){
			session.removeAttribute("item");
			return "/WEB-INF/jsp/ItemDetail.jsp";
		}
		return "/WEB-INF/jsp/DBerror.jsp";

	}

}
