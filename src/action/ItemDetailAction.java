package action;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import dao.DAOException;
import dao.ItemDAO;
import dto.DisposalidentificationNumberDTO;
import dto.ItemDTO;

public class ItemDetailAction implements Action {
	private int id = 0;

	@Override
	public boolean check(HttpServletRequest req) {
		String sid = req.getParameter("id");

		try {
			id = Integer.parseInt(sid);
		} catch (NumberFormatException e) {
			e.printStackTrace();
			return false;
		}

		return true;
	}

	@SuppressWarnings("unchecked")
	@Override
	public String execute(HttpServletRequest req) throws DAOException {
		HttpSession session = req.getSession(false);
		if (session != null) {
			ArrayList<ItemDTO> list = (ArrayList<ItemDTO>) session.getAttribute("item_dto");

			ItemDAO dao = new ItemDAO();
			ArrayList<DisposalidentificationNumberDTO> disNum = dao.DisposalidentNum(id);

			for (int i = 0; i < list.size(); i++) {
				ItemDTO dto = list.get(i);
				if (id == dto.getId()) {
					session.setAttribute("item", dto);
					session.setAttribute("disposal", disNum);
					return "/WEB-INF/jsp/ItemDetail.jsp";
				}
			}
		}
		return "/WEB-INF/jsp/error.jsp";
	}

}
