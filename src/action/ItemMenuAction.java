package action;

import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import dao.DAOException;
import dao.ItemDAO;
import dto.ItemDTO;

public class ItemMenuAction implements Action{

	@Override
	public boolean check(HttpServletRequest req) {
		return true;
	}

	@Override
	public String execute(HttpServletRequest req) throws DAOException {
		HttpSession session = req.getSession(false);

		ArrayList<ItemDTO> list = null;
		ItemDAO dao = new ItemDAO();
		try {
			// データ件数を取得する
			int dataNum = dao.getItemNum("0", "0", "");
			// セッションに結果を保存
			session.setAttribute("pageMax", (int)((dataNum + 9) / 10) );

			// 検索を行う
			list = dao.searchPage("0", "0", "", 1);
			System.out.println("ok" + list.size());
		} catch (SQLException e) {
			e.printStackTrace();
			return "/WEB-INF/jsp/error.jsp";
		}

		// 検索条件の保存
		session.setAttribute("searchCategory", "0");
		session.setAttribute("searchGenre", "0");
		session.setAttribute("searchString", "");
		req.setAttribute("page", 1);

		if (list.isEmpty()) {
			req.setAttribute("msg", "指定の条件では見つかりませんでした");
		}
		session.setAttribute("item_dto", list);
		return "/WEB-INF/jsp/ItemMenu.jsp";
	}
}
