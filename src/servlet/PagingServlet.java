package servlet;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.DAOException;
import dao.ItemDAO;
import dto.ItemDTO;

/**
 * Servlet implementation class PagingServlet
 */
@WebServlet("/paging")
public class PagingServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {
		req.setCharacterEncoding("UTF-8");
		// ページ番号を取得
		String pageStr = req.getParameter("page");
		
		
		System.out.println("come");
		// セッションから検索情報を取得
		HttpSession session = req.getSession();
		String searchCategoryStr = (String) session.getAttribute("searchCategory");
		String searchGenreStr = (String) session.getAttribute("searchGenre");
		String searchString = (String) session.getAttribute("searchString");
		int page;
		if(searchCategoryStr == null){
			searchCategoryStr = "0";
		}
		if(searchGenreStr == null){
			searchGenreStr = "0";
		}
		if(searchString == null){
			searchString = "";
		}
		if(pageStr == null || Integer.parseInt(pageStr) < 1){
			pageStr = "1";
			page = 1;
		}else{
			page = Integer.parseInt(pageStr);
		}
		
		// リクエストスコープに現在のページ番号を保存
		req.setAttribute("page", page);
		
		// 返却データ用のリスト
		ArrayList<ItemDTO> list = new ArrayList<>();
		
		try{
			ItemDAO dao = new ItemDAO();
			
			// 指定の条件で検索を行う
			list = dao.searchPage(searchCategoryStr, searchGenreStr, searchString, page);
			
			// セッションに結果を保存
			session.setAttribute("item_dto", list);
			
			// ビューへ転送
			req.getRequestDispatcher("/WEB-INF/jsp/ItemMenu.jsp").forward(req, res);

		}catch(SQLException | DAOException e){
			// エラーページへ転送
			req.getRequestDispatcher("/WEB-INF/jsp/error.jsp").forward(req, res);
		}
	}

}
