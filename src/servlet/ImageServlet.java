package servlet;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;

import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.ItemDAO;

/**
 * Servlet implementation class ImageServlet
 */
@WebServlet("/image")
public class ImageServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// 画像のIDを取得する
		String id = request.getParameter("id");

		// 取得したIDに紐づく画像データをDBから取得する
		BufferedImage img = null;
		try{
			ItemDAO dao = new ItemDAO();
			try{
				img = dao.selectImageById(Integer.parseInt(id));
			}catch(NullPointerException | NumberFormatException e){
				img = null;
			}

			if(img == null){
				img = ImageIO.read(new File("MMRental_Admin\\WebContent\\image\\NOIMAGE.jpg"));
			}

			// 画像をクライアントに返却する
			response.setContentType("image/jpeg");
			OutputStream os = response.getOutputStream();
			ImageIO.write(img, "jpg", os);
			os.flush();
		} catch (Exception e) {
			request.getRequestDispatcher("/WEB-INF/jsp/error.jsp").forward(request, response);
		}
	}

}
