package servlet;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import action.StaffMenuAction;
import dao.DAOException;
import dao.StaffDAO;

/**
 * Servlet implementation class DeleteStaffServlet
 */
@WebServlet("/deleteStaff")
public class DeleteStaffServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {

		req.setCharacterEncoding("UTF-8");
		String next = "";
		String msg = "";

		// IDデータ取得
		String id = req.getParameter("id");

		// DBから指定のIDデータを論理削除
		try {
			StaffDAO dao = new StaffDAO();
			dao.deleteStaff(id);

			msg += "ID" + id + "の従業員を削除しました";

		}catch (SQLException | DAOException e) {
			// DB関連エラー
			msg += "削除できませんでした";
		}

		// メッセージを格納
		req.setAttribute("errorMsg", msg);

		// スタッフ一覧へ移動させる
		try {
			next = new StaffMenuAction().execute(req);
		} catch (DAOException e) {
			e.printStackTrace();
		}
		req.getRequestDispatcher(next).forward(req,res);

	}

}
