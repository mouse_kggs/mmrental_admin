package servlet;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.DAOException;
import dao.RankingDAO;
import dto.RankingDTO;

/**
 * Servlet implementation class AnalyzeServlet
 */
@WebServlet("/analyze")
public class AnalyzeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {
		
		// 初期状態の検索内容を保存しておく
		req.setAttribute("category", 1);
		req.setAttribute("age", -1);
		
		// 初期状態としてカテゴリ1、年代は指定なしで検索を行う
		ArrayList<RankingDTO> list = new ArrayList<>();
		try {
			RankingDAO dao = new RankingDAO();
			list = dao.getRankingByCategory("1");
			
			// listに結果を保存
			req.setAttribute("list", list);

			// analyze.jspに遷移
			req.getRequestDispatcher("WEB-INF/jsp/analyze.jsp").forward(req, res);
		} catch (SQLException | DAOException e) {
			// エラーならエラーページへ
			req.getRequestDispatcher("/WEB-INF/jsp/DBerror.jsp").forward(req, res);
		}

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {
		
		// パラメータの取得
		String categoryStr = req.getParameter("category");
		String ageStr = req.getParameter("age");
		
		// 検索内容を保存しておく
		req.setAttribute("category", categoryStr);
		req.setAttribute("age", ageStr);
		
		// 指定の条件のランキングを取得してくる
		ArrayList<RankingDTO> list = new ArrayList<>();
		try{
			RankingDAO dao = new RankingDAO();
			
			// 年代指定なしならばカテゴリのみで検索
			if("-1".equals(ageStr)){
				list = dao.getRankingByCategory(categoryStr);
			}else{
				list = dao.getRankingByCategoryAndAge(categoryStr,ageStr);
			}
			
			// listに結果を保存
			req.setAttribute("list", list);
			
			// analyze.jspに遷移
			req.getRequestDispatcher("WEB-INF/jsp/analyze.jsp").forward(req, res);
			
		}catch(SQLException | DAOException e){
			// エラーならエラーページへ
			System.out.println("error");
			req.getRequestDispatcher("/WEB-INF/jsp/DBerror.jsp").forward(req, res);
		}
	}

}
