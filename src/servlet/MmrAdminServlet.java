package servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import action.Action;
import action.IdentNumDisposalAction;
import action.IdentNumDisposalCancelAction;
import action.ItemAddAcion;
import action.ItemAddScene;
import action.ItemDetailAction;
import action.ItemEditAction;
import action.ItemEditCancel;
import action.ItemMenuAction;
import action.ItemSearchAction;
import action.LoginAction;
import action.LogoutAction;
import action.PassUpadateAction;
import action.RentalCompleteAction;
import action.RentalDetailAction;
import action.RentalMenuAction;
import action.RentalSearchAction;
import action.ReturnCompleteAction;
import action.StaffMenuAction;
import dao.DAOException;
import dto.StaffDTO;

/**
 * Servlet implementation class MmrAdminServlet
 */
@WebServlet("/control")
public class MmrAdminServlet extends HttpServlet {
	String nextPage = null;

	protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		HttpSession session = req.getSession(false);
		if (session != null) {
			session.invalidate();
		}
		nextPage = "/WEB-INF/jsp/Login.jsp";
		RequestDispatcher rd = req.getRequestDispatcher(nextPage);
		rd.forward(req, res);
	}

	protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		req.setCharacterEncoding("UTF-8");
		RequestDispatcher rd = null;
		String actionName = null;
		actionName = req.getParameter("action");

		req.setAttribute("actionName", actionName);

		// 画面識別IDに応じたアクションを実行する
		if (!(actionName.equals("login")) && checkSession(req) == false) {

			nextPage = "/WEB-INF/jsp/Login.jsp";
			rd = req.getRequestDispatcher(nextPage);
		} else {
			Action action = createAction(actionName);
			if (action != null) {
				try {
					if (action.check(req)) {
						String path = action.execute(req);
						rd = req.getRequestDispatcher(path);
					} else {
						nextPage = "/WEB-INF/jsp/error.jsp";
						rd = req.getRequestDispatcher(nextPage);
					}
				} catch (DAOException e) {
					e.printStackTrace();
					nextPage = "/WEB-INF/jsp/DBerror.jsp";
					rd = req.getRequestDispatcher(nextPage);
				}
			}
		}
		rd.forward(req, res);
	}

	private boolean checkSession(HttpServletRequest req) {
		HttpSession session = req.getSession(false);
		if (session == null) {
			return false;
		}

		StaffDTO staff = (StaffDTO) session.getAttribute("login_dto");
		if (staff == null) {
			return false;
		} else {
			return true;
		}
	}

	private Action createAction(String name) {
		if (name.equals("login")) { // ログイン
			return new LoginAction();
		} else if (name.equals("passUpdate")) { // パスワード変更
			return new PassUpadateAction();
		} else if (name.equals("rentalAll")) { // レンタル一覧
			return new RentalMenuAction();
		} else if (name.equals("rentalSearcth")) { // レンタル検索
			return new RentalSearchAction();
		} else if (name.equals("itemMenu")) { // 商品マスタメンテナンス
			return new ItemMenuAction();
		} else if (name.equals("itemSearch")) { // 商品検索
			return new ItemSearchAction();
		} else if (name.equals("rentalDetail")) { // レンタル詳細
			return new RentalDetailAction();
		} else if (name.equals("returnUpdate")) { // 返却処理
			return new ReturnCompleteAction();
		} else if (name.equals("rentalUpdate")) { // 貸出処理
			return new RentalCompleteAction();
		} else if (name.equals("itemAddScene")) { // 商品追加画面遷移
			return new ItemAddScene();
		} else if (name.equals("itemAdd")) { // 商品追加登録
			return new ItemAddAcion();
		} else if (name.equals("itemDetail")) { // 商品詳細
			return new ItemDetailAction();
		} else if (name.equals("itemEdit")) { // 商品編集
			return new ItemEditAction();
		} else if (name.equals("itemEditCancel")) { // 商品編集
			return new ItemEditCancel();
		} else if (name.equals("disposal")) { // 廃番処理
			return new IdentNumDisposalAction();
		} else if (name.equals("disposalCancel")) { // 廃番キャンセル
			return new IdentNumDisposalCancelAction();
		} else if (name.equals("staffMenu")) { // 従業員マスタメンテナンス
			return new StaffMenuAction();
		} else if (name.equals("logout")) { // ログアウト
			return new LogoutAction();
		}
		return null;
	}

}
