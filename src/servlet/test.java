package servlet;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import dao.CommonDAO;
import dao.DAOException;

public class test extends CommonDAO {
	public static void main(String[] args) throws DAOException {
		System.out.println("TEST");
		init();

		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {
			pstmt = conn.prepareStatement("select user_name from user");
			rs = pstmt.executeQuery();

			while (rs.next()) {
				String name = rs.getString(1);
				System.out.print(name);
			}

		} catch (SQLException e) {
			throw new DAOException();
		} finally {
			destroy();
		}

	}
}
