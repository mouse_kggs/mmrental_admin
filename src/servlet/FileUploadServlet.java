package servlet;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import dao.DAOException;
import dao.UploadDao;
import dto.ItemDTO;
import dto.UploadDto;

/**
 * Servlet implementation class FileUploadServlet
 */
@WebServlet("/fileUpload")
public class FileUploadServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		req.setCharacterEncoding("UTF-8");

		ItemDTO itemDto = new ItemDTO();
		UploadDto dto = new UploadDto();
		for (Part p : req.getParts()) {
			if ("file".equals(p.getName())) {
				itemDto.setImage(p.getInputStream());
				//dto.setImage(p.getInputStream());
			} else if ("name".equals(p.getName())) {

				BufferedReader br = new BufferedReader(new InputStreamReader(p.getInputStream(), "UTF-8"));
				dto.setName(br.readLine());
			} else if ("id".equals(p.getName())) {
				BufferedReader br = new BufferedReader(new InputStreamReader(p.getInputStream()));
				dto.setId(Integer.parseInt(br.readLine()));
			}
		}

		UploadDao dao = new UploadDao();
		try {
			if (1 == dao.upload(dto)) {
				// �]��
				req.getRequestDispatcher("/WEB-INF/jsp/ItemMenu.jsp").forward(req, res);
			} else {
				System.out.println("error");
			}
		} catch (DAOException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}
	}
}
