package servlet;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import action.StaffCheck;
import action.StaffMenuAction;
import dao.DAOException;
import dao.IdExistException;
import dao.StaffDAO;
import dto.StaffDTO;

/**
 * Servlet implementation class UpdateStaffServlet
 */
@WebServlet("/updateStaff")
public class UpdateStaffServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {
		
		req.setCharacterEncoding("UTF-8");
		String next = "";

		StaffDTO dto = new StaffDTO();
		
		// 更新対象IDの取得
		String id = req.getParameter("id");
		if(id == null){
			// 不正なアクセスかも
			
		}
		// 入力項目が正しいかを判別し、エラーメッセージを取得
		String msg = StaffCheck.checkStaffData(req, dto);
		
		// エラーが無ければDBに新スタッフを追加
		if(msg.length() == 0){
			try {
				StaffDAO dao = new StaffDAO();
				dao.updateStaff(dto, id);
				
			}catch(IdExistException e){
				// ID重複エラー
				msg += "IDが重複しています<br>";
			}catch (SQLException | DAOException e) {
				// DB関連エラー
				// エラーページへの遷移
				req.getRequestDispatcher("/WEB-INF/jsp/error.jsp").forward(req, res);
			}
		}
		
		// メッセージを格納
		req.setAttribute("errorMsg", msg);
		
		// スタッフ一覧へ移動させる
		try {
			next = new StaffMenuAction().execute(req);
		} catch (DAOException e) {
			e.printStackTrace();
		}
		req.getRequestDispatcher(next).forward(req,res);
	}

}
