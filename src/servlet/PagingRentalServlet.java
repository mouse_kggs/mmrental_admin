package servlet;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.DAOException;
import dao.RentalDAO;
import dto.RentalDTO;

/**
 * Servlet implementation class PagingRentalServlet
 */
@WebServlet("/pagingRental")
public class PagingRentalServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {

		// セッションから検索条件を取得
		HttpSession session = req.getSession();
		RentalDTO dto = (RentalDTO)session.getAttribute("searchCondition");
		if(dto == null){
			dto = new RentalDTO("","","","","","");
		}

		// データ検索
		RentalDAO dao = new RentalDAO();
		ArrayList<RentalDTO> list;

		// ページ番号を取得
		String pageStr = req.getParameter("page");
		int page = 1;
		if(pageStr == null || Integer.parseInt(pageStr) < 1){
			pageStr = "1";
			page = 1;
		}else{
			page = Integer.parseInt(pageStr);
		}

		// 検索の実行
		try {
			list = dao.search(dto,page);
		} catch (DAOException e) {
			e.printStackTrace();
			req.getRequestDispatcher("/WEB-INF/jsp/error.jsp").forward(req, res);
			return;
		}
		if (list.isEmpty()) {
			String msg = "検索条件に該当するデータがありません。";
			req.setAttribute("errorMsg", msg);
		}
		session.setAttribute("rental_dto", list);
		req.setAttribute("page", page);
		req.getRequestDispatcher("/WEB-INF/jsp/RentalMenu.jsp").forward(req, res);

	}

}
