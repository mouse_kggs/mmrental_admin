package servlet;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import action.StaffMenuAction;
import dao.DAOException;
import dao.StaffDAO;

/**
 * Servlet implementation class ResetPasswordServlet
 */
@WebServlet("/resetPassword")
public class ResetPasswordServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {
		
		req.setCharacterEncoding("UTF-8");
		String next = "";
		String msg = "";

		// IDデータ取得
		String id = req.getParameter("id");
		
		// DBから指定のIDデータのパスワードをリセット
		try {
			StaffDAO dao = new StaffDAO();
			dao.resetPass(id);
			
			msg += "ID" + id + "のパスワードをリセットしました";
			
		}catch (SQLException | DAOException e) {
			// DB関連エラー
			msg += "リセットできませんでした";
		}
		
		// メッセージを格納
		req.setAttribute("errorMsg", msg);
		
		// スタッフ一覧へ移動させる
		try {
			next = new StaffMenuAction().execute(req);
		} catch (DAOException e) {
			e.printStackTrace();
		}
		req.getRequestDispatcher(next).forward(req,res);
		
	}

}
