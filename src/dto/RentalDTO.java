package dto;

import java.sql.Date;
import java.sql.Timestamp;

public class RentalDTO {
	private int rentalNum = 0;
	private int userId = 0;
	private Timestamp orderDate = null;
	private Date returnSchedule = null;
	private Date returnDate = null;
	private Date deliveryDate = null;
	private String deliveryTime = null;
	private int stock = 0;
	private String rentalid = null;
	private String returnid = null;
	private String status = null;
	private int age = 0;


	private String sOrderDate = null;
	private String sReturnDate = null;
	private String sReturnSchedule = null;

	public RentalDTO(int rentalNum,Timestamp order, Date returnSchedule, Date returnDate,
			int stock, String rentalid, String returnid, String status){
		setRentalNum(rentalNum);
		setOrderDate(order);
		setReturnSchedule(returnSchedule);
		setReturnDate(returnDate);
		setStock(stock);
		setRentalid(rentalid);
		setReturnid(returnid);
		setStatus(status);
	}

	public RentalDTO(Timestamp order, Date returnSchedule, Date returnDate,
			String rentalid, String returnid, String status){
		setOrderDate(order);
		setReturnSchedule(returnDate);
		setReturnDate(returnDate);
		setRentalid(rentalid);
		setReturnid(returnid);
		setStatus(status);
	}

	public RentalDTO(String sOrderDate, String returnSchedule, String sReturnDate,
			String rentalid, String returnid, String status){
		setsOrderDate(sOrderDate);
		setsReturnSchedule(returnSchedule);
		setsReturnDate(sReturnDate);
		setRentalid(rentalid);
		setReturnid(returnid);
		setStatus(status);
	}

	public RentalDTO(int rentalNum, String sOrderDate, String returnSchedule, String sReturnDate,
			int stock, String rentalid, String returnid, String status){
		setRentalNum(rentalNum);
		setsOrderDate(sOrderDate);
		setsReturnSchedule(returnSchedule);
		setsReturnDate(sReturnDate);
		setStock(stock);
		setRentalid(rentalid);
		setReturnid(returnid);
		setStatus(status);
	}

	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public String getsOrderDate() {
		return sOrderDate;
	}
	public void setsOrderDate(String sOrderDate) {
		this.sOrderDate = sOrderDate;
	}
	public String getsReturnDate() {
		return sReturnDate;
	}
	public void setsReturnDate(String sReturnDate) {
		this.sReturnDate = sReturnDate;
	}
	public String getsReturnSchedule() {
		return sReturnSchedule;
	}
	public void setsReturnSchedule(String sReturnSchedule) {
		this.sReturnSchedule = sReturnSchedule;
	}
	public int getRentalNum() {
		return rentalNum;
	}

	public void setRentalNum(int rentalNum) {
		this.rentalNum = rentalNum;
	}

	public int getId() {
		return userId;
	}
	public void setId(int id) {
		this.userId = id;
	}
	public Timestamp getOrderDate() {
		return orderDate;
	}
	public void setOrderDate(Timestamp orderDate) {
		this.orderDate = orderDate;
	}
	public Date getReturnSchedule() {
		return returnSchedule;
	}
	public void setReturnSchedule(Date returnSchedule) {
		this.returnSchedule = returnSchedule;
	}
	public Date getReturnDate() {
		return returnDate;
	}
	public void setReturnDate(Date returnDate) {
		this.returnDate = returnDate;
	}
	public Date getDeliveryDate() {
		return deliveryDate;
	}
	public void setDeliveryDate(Date deliveryDate) {
		this.deliveryDate = deliveryDate;
	}
	public String getDeliveryTime() {
		return deliveryTime;
	}
	public void setDeliveryTime(String deliveryTime) {
		this.deliveryTime = deliveryTime;
	}
	public int getStock() {
		return stock;
	}
	public void setStock(int stock) {
		this.stock = stock;
	}
	public String getRentalid() {
		return rentalid;
	}
	public void setRentalid(String rentalid) {
		this.rentalid = rentalid;
	}
	public String getReturnid() {
		return returnid;
	}
	public void setReturnid(String returnid) {
		this.returnid = returnid;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
}
