package dto;

public class DisposalidentificationNumberDTO {
	private int id;
	private int num;

	public DisposalidentificationNumberDTO(int id, int num) {
		setId(id);
		setNum(num);
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getNum() {
		return num;
	}

	public void setNum(int num) {
		this.num = num;
	}

}
