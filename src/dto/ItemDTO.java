package dto;

import java.io.InputStream;
import java.sql.Timestamp;

public class ItemDTO {
	private int id = 0;									//商品ID
	private String name = null;						//商品名
	private int categoryId = 0;						//カテゴリID
	private String category = null;					//カテゴリ名
	private int genreId = 0;							//ジャンルID
	private String genre = null;						//ジャンル名
	private boolean recommended = false;		//おすすめフラグ
	private int newAndOldId = 0;					//新旧区分ID
	private String newAndOld = null;				//新旧区分名
	private InputStream image = null;				//画像
	private  String artist = null;						//アーティスト
	private int price = 0;								//	値段
	private String remarks = null;					//その他
	private int maxIdentificationNum = 0;		//最大個体識別番号
	private Timestamp createDate = null;		//登録日時

	public ItemDTO(){

	}

	public ItemDTO(int newAndOld_id, int category_id, int genre_id, String title, String artist, int price, int maxIdentNum, boolean recommend, String remarks){
		setNewAndOldId(newAndOld_id);
		setCategoryId(category_id);
		setGenreId(genre_id);
		setName(title);
		setArtist(artist);
		setPrice(price);
		setMaxIdentificationNum(maxIdentNum);
		setRecommended(recommend);
		setRemarks(remarks);
	}

	public ItemDTO(String category, String genre, String name){
		setCategory(category);
		setGenre(genre);
		setName(name);
	}

	public ItemDTO(int id, String newAndOld, String category, InputStream image, String name, String artist, int price){
		setId(id);
		setNewAndOld(newAndOld);
		setCategory(category);
		setImage(image);
		setName(name);
		setArtist(artist);
		setPrice(price);
	}

	public ItemDTO(int id, String newAndOld, String category, String genre, String name, String artist, int price, int maxIdentNum, boolean recommended, String remarks){
		setId(id);
		setNewAndOld(newAndOld);
		setCategory(category);
		setGenre(genre);
		setName(name);
		setArtist(artist);
		setPrice(price);
		setMaxIdentificationNum(maxIdentNum);
		setRecommended(recommended);
		setRemarks(remarks);
	}

	public ItemDTO(int id, int newAndOld_id, int category_id, int genre_id, String title, String artist,
			int price, int maxIdentNum, boolean recommend, String remarks) {
		setId(id);
		setNewAndOldId(newAndOld_id);
		setCategoryId(category_id);
		setGenreId(genre_id);
		setName(title);
		setArtist(artist);
		setPrice(price);
		setMaxIdentificationNum(maxIdentNum);
		setRecommended(recommend);
		setRemarks(remarks);
	}

	public ItemDTO(int newAndOld_id, int category_id, int genre_id, InputStream image, String title, String artist, int price,
			int maxIdentNum, boolean recommend, String remarks) {
		setNewAndOldId(newAndOld_id);
		setCategoryId(category_id);
		setGenreId(genre_id);
		setImage(image);
		setName(title);
		setArtist(artist);
		setPrice(price);
		setMaxIdentificationNum(maxIdentNum);
		setRecommended(recommend);
		setRemarks(remarks);
	}

	public ItemDTO(int id, int newAndOld_id, int category_id, int genre_id, InputStream image, String title,
			String artist, int price, int maxIdentNum, boolean recommend, String remarks) {
		setId(id);
		setNewAndOldId(newAndOld_id);
		setCategoryId(category_id);
		setGenreId(genre_id);
		setImage(image);
		setName(title);
		setArtist(artist);
		setPrice(price);
		setMaxIdentificationNum(maxIdentNum);
		setRecommended(recommend);
		setRemarks(remarks);
	}

	public InputStream getImage() {
		return image;
	}


	public void setImage(InputStream image) {
		this.image = image;
	}


	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(int categoryId) {
		this.categoryId = categoryId;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public int getGenreId() {
		return genreId;
	}
	public void setGenreId(int genreId) {
		this.genreId = genreId;
	}
	public String getGenre() {
		return genre;
	}
	public void setGenre(String genre) {
		this.genre = genre;
	}
	public boolean getRecommended() {
		return recommended;
	}
	public void setRecommended(boolean recommended) {
		this.recommended = recommended;
	}
	public int getNewAndOldId() {
		return newAndOldId;
	}
	public void setNewAndOldId(int newAndOldId) {
		this.newAndOldId = newAndOldId;
	}
	public String getNewAndOld() {
		return newAndOld;
	}
	public void setNewAndOld(String newAndOld) {
		this.newAndOld = newAndOld;
	}
	public String getArtist() {
		return artist;
	}
	public void setArtist(String artist) {
		this.artist = artist;
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}
	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	public int getMaxIdentificationNum() {
		return maxIdentificationNum;
	}
	public void setMaxIdentificationNum(int maxIdentificationNum) {
		this.maxIdentificationNum = maxIdentificationNum;
	}
	public Timestamp getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Timestamp createDate) {
		this.createDate = createDate;
	}


}
