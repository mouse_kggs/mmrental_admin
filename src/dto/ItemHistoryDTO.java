package dto;

public class ItemHistoryDTO {
	private String category = null;
	private String genre = null;
	private String name = null;

	public ItemHistoryDTO(String category, String genre, String name){
		setCategory(category);
		setGenre(genre);
		setName(name);
	}


	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getGenre() {
		return genre;
	}
	public void setGenre(String genre) {
		this.genre = genre;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
}
