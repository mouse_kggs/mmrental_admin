package dto;

import java.sql.Blob;

public class RentalDetailDTO {
	private int rentNum = 0;
	private int itemid = 0;
	private String newAndOld = null;
	private String category = null;
	private String genre = null;
	private Blob image = null;
	private String name=null;
	private int identificationNum = 0;
	private boolean returnFlg = false;

	public RentalDetailDTO(int itemid, String newAndOld, String category, String genre, String name, int identificationNum, boolean returnFlg){
		setItemid(itemid);
		setNewAndOld(newAndOld);
		setCategory(category);
		setGenre(genre);
		setName(name);
		setIdentificationNum(identificationNum);
		setReturnFlg(returnFlg);
	}

	public RentalDetailDTO(int rentNum, int itemid, boolean returnFlg){
		setRentNum(rentNum);
		setItemid(itemid);
		setReturnFlg(returnFlg);
	}

	public int getRentNum() {
		return rentNum;
	}

	public void setRentNum(int rentNum) {
		this.rentNum = rentNum;
	}

	public int getItemid() {
		return itemid;
	}

	public void setItemid(int itemid) {
		this.itemid = itemid;
	}

	public boolean getReturnFlg() {
		return returnFlg;
	}

	public void setReturnFlg(boolean returnFlg) {
		this.returnFlg = returnFlg;
	}
	public String getNewAndOld() {
		return newAndOld;
	}
	public void setNewAndOld(String newAndOld) {
		this.newAndOld = newAndOld;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getGenre() {
		return genre;
	}
	public void setGenre(String genre) {
		this.genre = genre;
	}
	public Blob getImage() {
		return image;
	}
	public void setImage(Blob image) {
		this.image = image;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getIdentificationNum() {
		return identificationNum;
	}
	public void setIdentificationNum(int identificationNum) {
		this.identificationNum = identificationNum;
	}
}
