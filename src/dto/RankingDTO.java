package dto;

public class RankingDTO {

	int rank;
	String title;
	String artist;
	String category;
	String genre;
	int rental_num;
	int increment;
	int ratio;

	public RankingDTO() {

	}

	public RankingDTO(int rank, String title, String artist,String category, String genre, int rental_num, int increment, int ratio) {
		this.rank = rank;
		this.title = title;
		this.artist = artist;
		this.category = category;
		this.genre = genre;
		this.rental_num = rental_num;
		this.increment = increment;
		this.ratio = ratio;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public int getRank() {
		return rank;
	}

	public void setRank(int rank) {
		this.rank = rank;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getArtist() {
		return artist;
	}

	public void setArtist(String artist) {
		this.artist = artist;
	}

	public String getGenre() {
		return genre;
	}

	public void setGenre(String genre) {
		this.genre = genre;
	}

	public int getRental_num() {
		return rental_num;
	}

	public void setRental_num(int rental_num) {
		this.rental_num = rental_num;
	}

	public int getIncrement() {
		return increment;
	}

	public void setIncrement(int increment) {
		this.increment = increment;
	}

	public int getRatio() {
		return ratio;
	}

	public void setRatio(int ratio) {
		this.ratio = ratio;
	}

}
