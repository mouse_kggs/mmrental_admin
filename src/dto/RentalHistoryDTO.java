package dto;

public class RentalHistoryDTO {
	private String sOrderDate = null;
	private String sReturnDate = null;
	private String sReturnSchedule = null;
	private String rentalid = null;
	private String returnid = null;
	private String status = null;

	public RentalHistoryDTO(String sOrderDate, String sReturnDate, String sReturnSchedule, String rentalid, String returnid, String status){
		setsOrderDate(sOrderDate);
		setsReturnDate(sReturnDate);
		setsReturnSchedule(sReturnSchedule);
		setRentalid(rentalid);
		setReturnid(returnid);
		setStatus(status);
	}

	public String getsOrderDate() {
		return sOrderDate;
	}
	public void setsOrderDate(String sOrderDate) {
		this.sOrderDate = sOrderDate;
	}
	public String getsReturnDate() {
		return sReturnDate;
	}
	public void setsReturnDate(String sReturnDate) {
		this.sReturnDate = sReturnDate;
	}
	public String getsReturnSchedule() {
		return sReturnSchedule;
	}
	public void setsReturnSchedule(String sReturnSchedule) {
		this.sReturnSchedule = sReturnSchedule;
	}
	public String getRentalid() {
		return rentalid;
	}
	public void setRentalid(String rentalid) {
		this.rentalid = rentalid;
	}
	public String getReturnid() {
		return returnid;
	}
	public void setReturnid(String returnid) {
		this.returnid = returnid;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}

}
