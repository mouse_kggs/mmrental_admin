package dto;

public class StaffDTO {
	private String id = null;
	private String name = null;
	private String pass = null;
	private boolean manager_flg = false;
	private boolean del_flg = false;

	public StaffDTO(String id) {
		setId(id);
	}
	public StaffDTO() {
		getId();
		getName();
		getPass();
		getManager_flg();
		getDel_flg();
	}

	public StaffDTO(String id, String name, boolean manager_flg, boolean del_flg) {
		setId(id);
		setName(name);
		setManager_flg(manager_flg);
		setDel_flg(del_flg);
	}

	public StaffDTO(String id, boolean manager_flg) {
		setId(id);
		setManager_flg(manager_flg);
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPass() {
		return pass;
	}

	public void setPass(String pass) {
		this.pass = pass;
	}

	public boolean getManager_flg() {
		return manager_flg;
	}

	public void setManager_flg(boolean manager_flg) {
		this.manager_flg = manager_flg;
	}

	public boolean getDel_flg() {
		return del_flg;
	}

	public void setDel_flg(boolean del_flg) {
		this.del_flg = del_flg;
	}
}
