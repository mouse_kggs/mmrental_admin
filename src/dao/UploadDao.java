package dao;

import java.sql.PreparedStatement;
import java.sql.SQLException;

import dto.UploadDto;

public class UploadDao extends CommonDAO {

	public int upload(UploadDto dto) throws DAOException {
		init();

		StringBuffer sb = new StringBuffer();
		sb.append(" UPDATE");
		sb.append("        item");
		sb.append("    SET");
		sb.append("        item_name = ?");
		sb.append("      , image = ?");
		sb.append("  WHERE");
		sb.append("        item_id = ?");

		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {
			ps.setString(1, dto.getName());
			ps.setBinaryStream(2, dto.getImage());
			ps.setInt(3, dto.getId());

			return ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{
			destroy();
		}
		return 0;
	}

}
