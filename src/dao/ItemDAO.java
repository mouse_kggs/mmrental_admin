package dao;

import java.awt.image.BufferedImage;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.imageio.ImageIO;

import dto.DisposalidentificationNumberDTO;
import dto.ItemDTO;
import dto.RentalDetailDTO;

public class ItemDAO extends CommonDAO {

	public ItemDAO() {

	}

	public ArrayList<ItemDTO> all() throws DAOException {

		ResultSet rs = null;

		init();

		StringBuffer sb = new StringBuffer();
		sb.append("SELECT");
		sb.append("        ITEM_ID");
		sb.append("        ,NEW_AND_OLD_NAME");
		sb.append("        ,CATEGORY_NAME");
		sb.append("        ,GENRE_NAME");
		sb.append("        ,ITEM_NAME");
		sb.append("        ,ARTIST");
		sb.append("        ,PRICE");
		sb.append("        ,MAX_IDENTIFICATION_NUMBER");
		sb.append("        ,RECOMMENDED_FLG");
		sb.append("        ,REMARKS");
		sb.append("    FROM");
		sb.append("        ITEM INNER JOIN GENRE");
		sb.append("            ON GENRE.GENRE_ID = ITEM.GENRE_ID INNER JOIN CATEGORY");
		sb.append("            ON ITEM.CATEGORY_ID = CATEGORY.CATEGORY_ID INNER JOIN NEW_AND_OLD");
		sb.append("            ON NEW_AND_OLD.NEW_AND_OLD_ID = ITEM.NEW_AND_OLD_ID ");
		sb.append("ORDER BY");
		sb.append("    ITEM_ID;");
		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {
			rs = ps.executeQuery();

			ArrayList<ItemDTO> list = new ArrayList<ItemDTO>();

			while (rs.next()) {
				int id = rs.getInt(1);
				String newAndOld = rs.getString(2);
				String category = rs.getString(3);
				String genre = rs.getString(4);
				String name = rs.getString(5);
				String artist = rs.getString(6);
				int price = rs.getInt(7);
				int maxIdentNum = rs.getInt(8);
				boolean recommend = rs.getBoolean(9);
				String remarks = rs.getString(10);
				ItemDTO dto = new ItemDTO(id, newAndOld, category, genre, name, artist, price, maxIdentNum, recommend,
						remarks);
				list.add(dto);
			}

			return list;
		} catch (SQLException e) {
			e.printStackTrace();
			throw new DAOException();
		} finally {
			destroy();
		}
	}

	public ArrayList<ItemDTO> search(ItemDTO dto) throws DAOException {
		init();

		StringBuffer sb = new StringBuffer();
		sb.append("SELECT");
		sb.append("        ITEM_ID");
		sb.append("        ,NEW_AND_OLD_NAME");
		sb.append("        ,CATEGORY_NAME");
		sb.append("        ,GENRE_NAME");
		sb.append("        ,ITEM_NAME");
		sb.append("        ,ARTIST");
		sb.append("        ,PRICE");
		sb.append("        ,MAX_IDENTIFICATION_NUMBER");
		sb.append("        ,RECOMMENDED_FLG");
		sb.append("        ,REMARKS");
		sb.append("    FROM");
		sb.append(" ITEM INNER JOIN GENRE");
		sb.append("            ON GENRE.GENRE_ID = ITEM.GENRE_ID INNER JOIN CATEGORY");
		sb.append("            ON ITEM.CATEGORY_ID = CATEGORY.CATEGORY_ID INNER JOIN NEW_AND_OLD");
		sb.append("            ON NEW_AND_OLD.NEW_AND_OLD_ID = ITEM.NEW_AND_OLD_ID");
		sb.append(" WHERE");
		sb.append("    (CATEGORY_NAME LIKE ? OR CATEGORY_NAME IS NULL)");
		sb.append("    AND (GENRE_NAME LIKE ? OR GENRE_NAME IS NULL)");
		sb.append("    AND (ITEM_NAME LIKE ? OR ITEM_NAME IS NULL)");
		sb.append(" ORDER BY");
		sb.append("    ITEM_ID;");

		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {
			ps.setString(1, dto.getCategory() + "%");
			ps.setString(2, dto.getGenre() + "%");
			ps.setString(3, dto.getName() + "%");
			ResultSet rs = ps.executeQuery();

			ArrayList<ItemDTO> list = new ArrayList<ItemDTO>();

			while (rs.next()) {
				int id = rs.getInt(1);
				String newAndOld = rs.getString(2);
				String category = rs.getString(3);
				String genre = rs.getString(4);
				String name = rs.getString(5);
				String artist = rs.getString(6);
				int price = rs.getInt(7);
				int maxIdentNum = rs.getInt(8);
				boolean recommend = rs.getBoolean(9);
				String remarks = rs.getString(10);
				dto = new ItemDTO(id, newAndOld, category, genre, name, artist, price, maxIdentNum, recommend, remarks);
				list.add(dto);
			}

			return list;
		} catch (SQLException e) {
			e.printStackTrace();
			throw new DAOException();
		} finally {
			destroy();
		}
	}

	public ArrayList<RentalDetailDTO> rentalDetail(int id) throws DAOException {
		init();

		StringBuffer sb = new StringBuffer();
		sb.append("SELECT");
		sb.append("        ITEM.ITEM_ID");
		sb.append("        ,NEW_AND_OLD_NAME");
		sb.append("        ,CATEGORY_NAME");
		sb.append("        ,GENRE_NAME");
		sb.append("        ,ITEM_NAME");
		sb.append("        ,IDENTIFICATION_NUMBER");
		sb.append("        ,RETURN_FLG");
		sb.append("    FROM");
		sb.append("        RENTAL_DETAIL INNER JOIN ITEM");
		sb.append("            ON RENTAL_DETAIL.ITEM_ID = ITEM.ITEM_ID INNER JOIN GENRE");
		sb.append("            ON ITEM.GENRE_ID = GENRE.GENRE_ID INNER JOIN CATEGORY");
		sb.append("            ON ITEM.CATEGORY_ID = CATEGORY.CATEGORY_ID INNER JOIN NEW_AND_OLD");
		sb.append("            ON NEW_AND_OLD.NEW_AND_OLD_ID = ITEM.NEW_AND_OLD_ID");
		sb.append(" WHERE");
		sb.append("    RENTAL_NUMBER = ?");
		sb.append(" ORDER BY");
		sb.append("    ITEM_ID;");

		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {
			ps.setInt(1, id);
			ResultSet rs = ps.executeQuery();

			ArrayList<RentalDetailDTO> list = new ArrayList<RentalDetailDTO>();

			while (rs.next()) {
				int itemid = rs.getInt(1);
				String newAndOld = rs.getString(2);
				String category = rs.getString(3);
				String genre = rs.getString(4);
				String name = rs.getString(5);
				int identificationNum = rs.getInt(6);
				boolean flg = rs.getBoolean(7);
				RentalDetailDTO dto = new RentalDetailDTO(itemid, newAndOld, category, genre, name, identificationNum,
						flg);
				list.add(dto);
			}

			return list;
		} catch (SQLException e) {
			e.printStackTrace();
			throw new DAOException();
		} finally {
			destroy();
		}
	}

	/**
	 * 画像データを取得するメソッド
	 *
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public BufferedImage selectImageById(int id) throws Exception {
		init();
		try {
			// SQL文を作成する
			StringBuffer sb = new StringBuffer();
			sb.append("   select");
			sb.append("          image");
			sb.append("     from item");
			sb.append("    where item_id = ?");

			// SQL文を実行する
			PreparedStatement ps = conn.prepareStatement(sb.toString());
			ps.setInt(1, id);
			ResultSet rs = ps.executeQuery();

			// 結果セットから画像データを取得し、返却する
			if (rs.next()) {
				InputStream is = rs.getBinaryStream("image");
				BufferedInputStream bis = new BufferedInputStream(is);
				return ImageIO.read(bis);
			}

		} catch (IOException | SQLException e) {
			// e.printStackTrace();
			throw e;
		} finally {
			destroy();
		}
		return null;
	}

	public int add(ItemDTO dto) throws DAOException {
		init();

		StringBuffer sb = new StringBuffer();
		sb.append("INSERT");
		sb.append("    INTO");
		sb.append("        item(");
		sb.append("            new_and_old_id");
		sb.append("            ,category_id");
		sb.append("            ,genre_id");
		sb.append("			,image");
		sb.append("            ,item_name");
		sb.append("            ,artist");
		sb.append("            ,price");
		sb.append("            ,max_identification_number");
		sb.append("            ,recommended_flg");
		sb.append("            ,remarks");
		sb.append("        )");
		sb.append("    VALUES");
		sb.append("        (");
		sb.append("            ?");
		sb.append("            ,?");
		sb.append("            ,?");
		sb.append("            ,?");
		sb.append("            ,?");
		sb.append("            ,?");
		sb.append("            ,?");
		sb.append("            ,?");
		sb.append("            ,?");
		sb.append("            ,?");
		sb.append("        );");

		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {
			conn.setAutoCommit(false);

			ps.setInt(1, dto.getNewAndOldId());
			ps.setInt(2, dto.getCategoryId());
			ps.setInt(3, dto.getGenreId());
			ps.setBinaryStream(4, dto.getImage());
			ps.setString(5, dto.getName());
			ps.setString(6, dto.getArtist());
			ps.setInt(7, dto.getPrice());
			ps.setInt(8, dto.getMaxIdentificationNum());
			ps.setBoolean(9, dto.getRecommended());
			ps.setString(10, dto.getRemarks());

			int result = ps.executeUpdate();

			if (result == 1) {
				conn.commit();
				return result;
			}

			return result;
		} catch (SQLException e) {
			e.printStackTrace();
			try {
				conn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			throw new DAOException();
		} finally {
			destroy();
		}
	}

	public int edit(ItemDTO dto) throws DAOException {
		init();

		StringBuffer sb = new StringBuffer();
		sb.append("UPDATE");
		sb.append("    ITEM");
		sb.append("        SET");
		sb.append("            NEW_AND_OLD_ID = ?");
		sb.append("            ,CATEGORY_ID = ?");
		sb.append("            ,GENRE_ID = ?");
		sb.append("            ,ITEM_NAME = ?");
		sb.append("            ,ARTIST = ?");
		sb.append("            ,PRICE = ?");
		sb.append("            ,MAX_IDENTIFICATION_NUMBER = ?");
		sb.append("            ,RECOMMENDED_FLG = ?");
		sb.append("            ,REMARKS = ?");
		sb.append("    WHERE");
		sb.append("    		ITEM_ID = ?");

		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {
			conn.setAutoCommit(false);

			ps.setInt(1, dto.getNewAndOldId());
			ps.setInt(2, dto.getCategoryId());
			ps.setInt(3, dto.getGenreId());
			ps.setString(4, dto.getName());
			ps.setString(5, dto.getArtist());
			ps.setInt(6, dto.getPrice());
			ps.setInt(7, dto.getMaxIdentificationNum());
			ps.setBoolean(8, dto.getRecommended());
			ps.setString(9, dto.getRemarks());
			ps.setInt(10, dto.getId());

			int result = ps.executeUpdate();

			if (result == 1) {
				conn.commit();
				return result;
			}

			return result;
		} catch (SQLException e) {
			e.printStackTrace();
			try {
				conn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			throw new DAOException();
		} finally {
			destroy();
		}
	}

	public int editnImage(ItemDTO dto) throws DAOException {
		init();

		StringBuffer sb = new StringBuffer();
		sb.append("UPDATE");
		sb.append("    ITEM");
		sb.append("        SET");
		sb.append("            NEW_AND_OLD_ID = ?");
		sb.append("            ,CATEGORY_ID = ?");
		sb.append("            ,GENRE_ID = ?");
		sb.append("            ,IMAGE = ?");
		sb.append("            ,ITEM_NAME = ?");
		sb.append("            ,ARTIST = ?");
		sb.append("            ,PRICE = ?");
		sb.append("            ,MAX_IDENTIFICATION_NUMBER = ?");
		sb.append("            ,RECOMMENDED_FLG = ?");
		sb.append("            ,REMARKS = ?");
		sb.append("    WHERE");
		sb.append("    		ITEM_ID = ?");

		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {
			conn.setAutoCommit(false);

			ps.setInt(1, dto.getNewAndOldId());
			ps.setInt(2, dto.getCategoryId());
			ps.setInt(3, dto.getGenreId());
			ps.setBinaryStream(4, dto.getImage());
			ps.setString(5, dto.getName());
			ps.setString(6, dto.getArtist());
			ps.setInt(7, dto.getPrice());
			ps.setInt(8, dto.getMaxIdentificationNum());
			ps.setBoolean(9, dto.getRecommended());
			ps.setString(10, dto.getRemarks());
			ps.setInt(11, dto.getId());

			int result = ps.executeUpdate();

			if (result == 1) {
				conn.commit();
				return result;
			}

			return result;
		} catch (SQLException e) {
			e.printStackTrace();
			try {
				conn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			throw new DAOException();
		} finally {
			destroy();
		}
	}

	public ArrayList<ItemDTO> searchPage(String category, String genre, String searchString, int page)
			throws SQLException, DAOException {
		init();
		StringBuffer sb = new StringBuffer();
		sb.append("SELECT");
		sb.append("        item_id");
		sb.append("        ,new_and_old_name");
		sb.append("        ,category_name");
		sb.append("        ,item_name");
		sb.append("        ,artist");
		sb.append("        ,price");
		sb.append("        ,max_identification_number");
		sb.append("        ,recommended_flg");
		sb.append("        ,item.category_id");
		sb.append("        ,item.genre_id");
		sb.append("    FROM");
		sb.append("        item INNER JOIN new_and_old");
		sb.append("            ON item.new_and_old_id = new_and_old.new_and_old_id INNER JOIN category");
		sb.append("            ON item.category_id = category.category_id");
		sb.append(" WHERE");
		sb.append("    item_name LIKE ?");
		if (!"0".equals(category)) {
			sb.append("    AND item.category_id LIKE ?");
		}
		if (!"0".equals(genre)) {
			sb.append("    AND item.genre_id LIKE ?");
		}
		sb.append(" ORDER BY");
		sb.append("    item_id DESC limit 10 offset ?");

		ArrayList<ItemDTO> itemList = new ArrayList<>();

		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {

			// ?に値をセットして実行
			int i = 1;
			ps.setString(i++, "%" + searchString + "%");
			if (!"0".equals(category)) {
				ps.setString(i++, category);
			}
			if (!"0".equals(genre)) {
				ps.setString(i++, genre);
			}
			ps.setInt(i, (page - 1) * 10);
			ResultSet rs = ps.executeQuery();

			// データを一件ずつEmpDTOの形にしてリストへ追加
			while (rs.next()) {
				ItemDTO item = new ItemDTO();
				item.setId(rs.getInt("item_id"));
				item.setNewAndOld(rs.getString("new_and_old_name"));
				item.setCategory(rs.getString("category_name"));
				item.setName(rs.getString("item_name"));
				item.setArtist(rs.getString("artist"));
				item.setPrice(rs.getInt("price"));
				item.setMaxIdentificationNum(rs.getInt("max_identification_number"));
				item.setRecommended(rs.getBoolean("recommended_flg"));
				item.setCategoryId(rs.getInt("category_id"));
				item.setGenreId(rs.getInt("genre_id"));
				itemList.add(item);
			}
			return itemList;

		} catch (SQLException e) {
			throw e;
		} finally {
			destroy();
		}
	}

	public int getItemNum(String category, String genre, String searchString) throws SQLException, DAOException {
		init();
		StringBuffer sb = new StringBuffer();
		sb.append("SELECT");
		sb.append("        count(*)");
		sb.append("    FROM");
		sb.append("        item INNER JOIN new_and_old");
		sb.append("            ON item.new_and_old_id = new_and_old.new_and_old_id INNER JOIN category");
		sb.append("            ON item.category_id = category.category_id");
		sb.append(" WHERE");
		sb.append("    item_name LIKE ?");
		if (!"0".equals(category)) {
			sb.append("    AND item.category_id LIKE ?");
		}
		if (!"0".equals(genre)) {
			sb.append("    AND item.genre_id LIKE ?");
		}

		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {

			// ?に値をセットして実行
			int i = 1;
			ps.setString(i++, "%" + searchString + "%");
			if (!"0".equals(category)) {
				ps.setString(i++, category);
			}
			if (!"0".equals(genre)) {
				ps.setString(i++, genre);
			}
			ResultSet rs = ps.executeQuery();

			// 取得したデータの数を返却
			if (rs.next()) {
				return rs.getInt(1);
			}

			throw new SQLException();

		} catch (SQLException e) {
			throw e;
		} finally {
			destroy();
		}
	}

	public ArrayList<DisposalidentificationNumberDTO> DisposalidentNum(int id) throws DAOException {
		init();

		StringBuffer sb = new StringBuffer();
		sb.append("SELECT");
		sb.append("        *");
		sb.append("    FROM");
		sb.append("        disposal_identification_number");
		sb.append("    WHERE");
		sb.append("        item_id = ?;");

		try (PreparedStatement ps = conn.prepareCall(sb.toString())) {
			ps.setInt(1, id);

			ResultSet rs = ps.executeQuery();

			ArrayList<DisposalidentificationNumberDTO> list = new ArrayList<>();

			while (rs.next()) {
				id = rs.getInt(1);
				int num = rs.getInt(2);
				DisposalidentificationNumberDTO dto = new DisposalidentificationNumberDTO(id, num);
				list.add(dto);
			}

			return list;
		} catch (SQLException e) {
			e.printStackTrace();
			throw new DAOException();
		} finally {
			destroy();
		}
	}

	public int disNum(int id, int num) throws DAOException {
		init();

		StringBuffer sb = new StringBuffer();
		sb.append("INSERT");
		sb.append("    INTO");
		sb.append("        disposal_identification_number");
		sb.append("    VALUES");
		sb.append("        (");
		sb.append("            ?");
		sb.append("            ,?");
		sb.append("        );");

		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {
			conn.setAutoCommit(false);

			ps.setInt(1, id);
			ps.setInt(2, num);

			int result = ps.executeUpdate();

			if (result == 1) {
				conn.commit();
				return result;
			}
			return result;
		} catch (SQLException e) {
			e.printStackTrace();
			throw new DAOException();
		} finally {
			destroy();
		}
	}

	public int disNumCancel(int id, int num) throws DAOException {
		init();

		StringBuffer sb = new StringBuffer();
		sb.append("DELETE");
		sb.append("    FROM");
		sb.append("        disposal_identification_number");
		sb.append("    WHERE");
		sb.append("            item_id = ?");
		sb.append("            AND identification_number = ?;");

		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {
			conn.setAutoCommit(false);

			ps.setInt(1, id);
			ps.setInt(2, num);

			int result = ps.executeUpdate();

			if (result == 1) {
				conn.commit();
				return result;
			}
			return result;
		} catch (SQLException e) {
			e.printStackTrace();
			throw new DAOException();
		} finally {
			destroy();
		}
	}

}
