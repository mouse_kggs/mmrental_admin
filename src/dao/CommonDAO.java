package dao;

import java.sql.Connection;
import java.sql.SQLException;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;



public class CommonDAO {
	protected static Connection conn = null;

	protected static void init() throws DAOException{
		try {
			Context ctx =new InitialContext();
			DataSource ds = (DataSource)ctx.lookup("java:comp/env/jdbc/mysql/mmr");
			conn = ds.getConnection();
		} catch (SQLException | NamingException e) {
			e.printStackTrace();
			throw new DAOException();
		}
	}

	protected static void destroy() throws DAOException{
		try{
		    if (conn != null){
		      conn.close();
		    }
		  }catch (SQLException e){
		    e.printStackTrace();
		    throw new DAOException();
		  }
	}
	
	/**
	 * コネクションを取得するメソッド
	 * @return
	 * @throws NamingException
	 * @throws SQLException
	 */
	public static Connection getConnection() throws NamingException, SQLException{
		Context ctx = new InitialContext();
		DataSource ds = (DataSource)ctx.lookup("java:comp/env/jdbc/mysql/mmr");
		return ds.getConnection();
	}
}
