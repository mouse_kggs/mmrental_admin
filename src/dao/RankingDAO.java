package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import dto.RankingDTO;

public class RankingDAO extends CommonDAO {

	public ArrayList<RankingDTO> getRankingByCategory(String category_id) throws SQLException, DAOException {
		init();

		// SQL文の作成
		StringBuffer sb = new StringBuffer();
		sb.append("SELECT");
		sb.append("        item.item_id");
		sb.append("        ,item_name");
		sb.append("        ,artist");
		sb.append("        ,category_name");
		sb.append("        ,genre_name");
		sb.append("        ,COUNT(*) AS rental_num");
		sb.append("        ,COUNT(*) - last_num AS incre");
		sb.append("        ,COUNT(*) / last_num * 100 AS ratio");
		sb.append("    FROM");
		sb.append("        item INNER JOIN rental_detail");
		sb.append("            ON item.item_id = rental_detail.item_id INNER JOIN rental");
		sb.append("            ON rental_detail.rental_number = rental.rental_number INNER JOIN genre");
		sb.append("            ON item.genre_id = genre.genre_id INNER JOIN category");
		sb.append("            ON item.category_id = category.category_id");

		sb.append("        LEFT JOIN (");
		sb.append("            SELECT");
		sb.append("                    item_id");
		sb.append("                    ,COUNT(*) AS last_num");
		sb.append("                FROM");
		sb.append("                    rental");
		sb.append("                        LEFT JOIN rental_detail");
		sb.append("                            ON rental.rental_number = rental_detail.rental_number");
		sb.append("                WHERE");
		sb.append("                    order_datetime BETWEEN date_sub(");
		sb.append("                        now()");
		sb.append("                        ,INTERVAL 14 day");
		sb.append("                    ) AND date_sub(");
		sb.append("                        now()");
		sb.append("                        ,INTERVAL 8 day");
		sb.append("                    )");
		sb.append("                GROUP BY");
		sb.append("                    item_id");
		sb.append("        ) AS last_data");
		sb.append("            ON item.item_id = last_data.item_id");
		sb.append(" WHERE");
		sb.append("    order_datetime BETWEEN date_sub(");
		sb.append("        now()");
		sb.append("        ,INTERVAL 7 day");
		sb.append("    ) AND date_sub(");
		sb.append("        now()");
		sb.append("        ,INTERVAL 0 day");
		sb.append("    )");
		if ("-1".equals(category_id)) {
			category_id = "2";
			sb.append("    AND (item.category_id like ?");
			sb.append("        OR item.category_id = '3')");
		} else {
			sb.append("    AND item.category_id LIKE ?");
		}
		sb.append(" GROUP BY");
		sb.append("    item.item_id");
		sb.append(" ORDER BY");
		sb.append("    COUNT(*) DESC");
		sb.append(" limit 200");

		// 結果返却用のリストを用意
		ArrayList<RankingDTO> rankingList = new ArrayList<>();

		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {
			// ?に値をセットして実行
			ps.setString(1, "%" + category_id);
			ResultSet rs = ps.executeQuery();

			// データを一件ずつEmpDTOの形にしてリストへ追加
			int num = 1;
			while (rs.next()) {
				RankingDTO ranking = new RankingDTO();
				ranking.setRank(num++);
				ranking.setTitle(rs.getString("item_name"));
				ranking.setArtist(rs.getString("artist"));
				ranking.setCategory(rs.getString("category_name"));
				ranking.setGenre(rs.getString("genre_name"));
				ranking.setRental_num(rs.getInt("rental_num"));
				ranking.setIncrement(rs.getInt("incre"));
				ranking.setRatio(rs.getInt("ratio"));
				rankingList.add(ranking);
			}
			return rankingList;

		} catch (SQLException e) {
			throw e;
		} finally {
			destroy();
		}

	}

	public ArrayList<RankingDTO> getRankingByCategoryAndAge(String category_id, String age)
			throws SQLException, DAOException {
		init();

		// SQL文の作成
		StringBuffer sb = new StringBuffer();
		sb.append("SELECT");
		sb.append("        item.ITEM_ID");
		sb.append("        ,item_name");
		sb.append("        ,artist");
		sb.append("        ,category_name");
		sb.append("        ,genre_name");
		sb.append("        ,COUNT(*) AS rental_num");
		sb.append("        ,COUNT(*) - last_num AS incre");
		sb.append("        ,COUNT(*) / last_num * 100 AS ratio");
		sb.append("        ,age");
		sb.append("    FROM");
		sb.append("        item INNER JOIN rental_detail");
		sb.append("            ON item.item_id = rental_detail.item_id INNER JOIN rental");
		sb.append("            ON rental_detail.rental_number = rental.rental_number INNER JOIN genre");
		sb.append("            ON item.genre_id = genre.genre_id INNER JOIN category");
		sb.append("            ON item.category_id = category.category_id");

		sb.append("        LEFT JOIN (");
		sb.append("            SELECT");
		sb.append("                    item_id");
		sb.append("                    ,COUNT(*) AS last_num");
		sb.append("                FROM");
		sb.append("                    rental");
		sb.append("                        LEFT JOIN rental_detail");
		sb.append("                            ON rental.rental_number = rental_detail.rental_number");
		sb.append("                WHERE");
		sb.append("                    order_datetime BETWEEN date_sub(");
		sb.append("                        now()");
		sb.append("                        ,INTERVAL 14 day");
		sb.append("                    ) AND date_sub(");
		sb.append("                        now()");
		sb.append("                        ,INTERVAL 8 day");
		sb.append("                    )");
		sb.append("                    AND age LIKE ?");
		sb.append("                GROUP BY");
		sb.append("                    item_id");
		sb.append("                    ,age");
		sb.append("        ) AS last_data");
		sb.append("            ON item.item_id = last_data.item_id");
		sb.append(" WHERE");
		sb.append("    order_datetime BETWEEN date_sub(");
		sb.append("        now()");
		sb.append("        ,INTERVAL 7 day");
		sb.append("    ) AND date_sub(");
		sb.append("        now()");
		sb.append("        ,INTERVAL 0 day");
		sb.append("    )");
		if ("-1".equals(category_id)) {
			category_id = "2";
			sb.append("    AND (item.category_id like ?");
			sb.append("        OR item.category_id = '3')");
		} else {
			sb.append("    AND item.category_id LIKE ?");
		}
		sb.append("    AND age LIKE ?");
		sb.append(" GROUP BY");
		sb.append("    item.item_id");
		sb.append("    ,age");
		sb.append(" ORDER BY");
		sb.append("    COUNT(*) DESC limit 200");

		// 結果返却用のリストを用意
		ArrayList<RankingDTO> rankingList = new ArrayList<>();

		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {
			// ?に値をセットして実行
			ps.setString(1, "%" + age);
			ps.setString(2, "%" + category_id);
			ps.setString(3, "%" + age);
			ResultSet rs = ps.executeQuery();

			// データを一件ずつEmpDTOの形にしてリストへ追加
			int num = 1;
			while (rs.next()) {
				RankingDTO ranking = new RankingDTO();
				ranking.setRank(num++);
				ranking.setTitle(rs.getString("item_name"));
				ranking.setArtist(rs.getString("artist"));
				ranking.setCategory(rs.getString("category_name"));
				ranking.setGenre(rs.getString("genre_name"));
				ranking.setRental_num(rs.getInt("rental_num"));
				ranking.setIncrement(rs.getInt("incre"));
				ranking.setRatio(rs.getInt("ratio"));
				rankingList.add(ranking);
			}
			return rankingList;

		} catch (SQLException e) {
			throw e;
		} finally {
			destroy();
		}
	}
}
