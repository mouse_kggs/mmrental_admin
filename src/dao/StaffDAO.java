package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;

import dto.StaffDTO;

public class StaffDAO extends CommonDAO {
	public boolean search(String id, String pass, StaffDTO dto) throws DAOException {
		init();

		StringBuffer sb = new StringBuffer();
		sb.append("SELECT");
		sb.append("        staff_id");
		sb.append("        ,password");
		sb.append("        ,manager_flg");
		sb.append("    FROM");
		sb.append("        staff");
		sb.append("    WHERE");
		sb.append("        staff_id = ?");
		sb.append("        AND password = password(?);");

		ResultSet rs = null;

		try(PreparedStatement ps = conn.prepareStatement(sb.toString())) {

			ps.setString(1, id);
			ps.setString(2, pass);
			rs = ps.executeQuery();
			boolean flg = false;
			boolean check = false;
			if (rs.next()) {
				 flg = rs.getBoolean(3);
				dto.setManager_flg(flg);
				check = true;
			}

			return check;
		} catch (SQLException e) {
			e.printStackTrace();
			throw new DAOException();
		} finally {
			destroy();
		}
	}

	public int passUpdate(String id, String pass) throws DAOException {
		init();

		StringBuffer sb = new StringBuffer();
		sb.append("UPDATE");
		sb.append("        STAFF");
		sb.append("    SET");
		sb.append("        PASSWORD = PASSWORD(?)");
		sb.append("    WHERE");
		sb.append("        STAFF_ID = ?;");


		try(PreparedStatement ps = conn.prepareStatement(sb.toString())) {
			ps.setString(1, pass);
			ps.setString(2, id);
			int result = ps.executeUpdate();

			if (result == 1) {
				return result;
			}

			return result;
		} catch (SQLException e) {
			e.printStackTrace();
			throw new DAOException();
		} finally {
			destroy();
		}
	}

	public ArrayList<StaffDTO> all() throws DAOException {
		init();

		StringBuffer sb = new StringBuffer();
		sb.append("SELECT");
		sb.append("        *");
		sb.append("    FROM");
		sb.append("        STAFF;");

		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {
			ResultSet rs = ps.executeQuery();

			ArrayList<StaffDTO> list = new ArrayList<StaffDTO>();

			while (rs.next()) {
				String id = rs.getString(1);
				String name = rs.getString(3);
				boolean manage_flg = rs.getBoolean(4);
				boolean del_flg = rs.getBoolean(5);
				StaffDTO dto = new StaffDTO(id, name, manage_flg, del_flg);
				list.add(dto);
			}

			return list;
		} catch (SQLException e) {
			e.printStackTrace();
			throw new DAOException();
		} finally {
			destroy();
		}
	}

	public ArrayList<StaffDTO> selectUndeletedStaff() throws DAOException {
		init();

		StringBuffer sb = new StringBuffer();
		sb.append("SELECT");
		sb.append("        *");
		sb.append("    FROM");
		sb.append("        staff");
		sb.append("    WHERE");
		sb.append("        delete_flg = false");

		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {
			ResultSet rs = ps.executeQuery();

			ArrayList<StaffDTO> list = new ArrayList<StaffDTO>();

			while (rs.next()) {
				String id = rs.getString(1);
				String name = rs.getString(3);
				boolean manage_flg = rs.getBoolean(4);
				boolean del_flg = rs.getBoolean(5);
				StaffDTO dto = new StaffDTO(id, name, manage_flg, del_flg);
				list.add(dto);
			}

			return list;
		} catch (SQLException e) {
			e.printStackTrace();
			throw new DAOException();
		} finally {
			destroy();
		}
	}

	public void addStaff(StaffDTO data) throws DAOException, SQLException, IdExistException {
		init();

		// SQL文の作成
		StringBuffer sb = new StringBuffer();
		sb.append("INSERT");
		sb.append("    INTO");
		sb.append("        staff");
		sb.append("    VALUES");
		sb.append("        (");
		sb.append("            ?");
		sb.append("            ,PASSWORD(?)");
		sb.append("            ,?");
		sb.append("            ,?");
		sb.append("            ,?");
		sb.append("        )");

		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {
			ps.setString(1, data.getId());
			ps.setString(2, data.getPass());
			ps.setString(3, data.getName());
			ps.setBoolean(4, data.getManager_flg());
			ps.setBoolean(5, data.getDel_flg());

			// SQL文の実行
			ps.executeUpdate();
		} catch (MySQLIntegrityConstraintViolationException e) {
			throw new IdExistException();
		} finally {
			destroy();
		}

	}

	public void updateStaff(StaffDTO data, String id) throws DAOException, SQLException, IdExistException {
		init();

		// SQL文の作成
		StringBuffer sb = new StringBuffer();
		sb.append("UPDATE");
		sb.append("        staff");
		sb.append("    SET");
		sb.append("        staff_id = ?");
		sb.append("        ,staff_name = ?");
		sb.append("        ,manager_flg = ?");
		sb.append("    WHERE");
		sb.append("        staff_id = ?;");

		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {
			ps.setString(1, data.getId());
			ps.setString(2, data.getName());
			ps.setBoolean(3, data.getManager_flg());
			ps.setString(4, id);

			// SQL文の実行
			ps.executeUpdate();
		} catch (MySQLIntegrityConstraintViolationException e) {
			throw new IdExistException();
		} finally {
			destroy();
		}
	}

	public void deleteStaff(String id) throws DAOException, SQLException {
		init();

		// SQL文の作成
		StringBuffer sb = new StringBuffer();
		sb.append("UPDATE");
		sb.append("        staff");
		sb.append("    SET");
		sb.append("        delete_flg = true");
		sb.append("    WHERE");
		sb.append("        staff_id = ?;");

		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {
			ps.setString(1, id);

			// SQL文の実行
			ps.executeUpdate();
		} finally {
			destroy();
		}
	}

	public void resetPass(String id) throws DAOException, SQLException {
		init();

		// SQL文の作成
		StringBuffer sb = new StringBuffer();
		sb.append("UPDATE");
		sb.append("        staff");
		sb.append("    SET");
		sb.append("        password = PASSWORD('default')");
		sb.append("    WHERE");
		sb.append("        staff_id = ?;");

		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {
			ps.setString(1, id);

			// SQL文の実行
			ps.executeUpdate();
		} finally {
			destroy();
		}
	}
}
