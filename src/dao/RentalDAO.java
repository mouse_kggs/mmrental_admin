package dao;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;

import dto.RentalDTO;

public class RentalDAO extends CommonDAO {
	public ArrayList<RentalDTO> all() throws DAOException {
		ResultSet rs = null;

		ArrayList<RentalDTO> list = null;

		init();

		StringBuffer sb = new StringBuffer();
		sb.append("SELECT");
		sb.append("        RENTAL_DETAIL.RENTAL_NUMBER");
		sb.append("        ,ORDER_DATETIME");
		sb.append("        ,RETURN_SCHEDULE");
		sb.append("        ,RETURN_DATETIME");
		sb.append("        ,COUNT(*) AS '数量'");
		sb.append("        ,A1.STAFF_NAME");
		sb.append("        ,A2.STAFF_NAME");
		sb.append("        ,STATUS_NAME");
		sb.append("    FROM");
		sb.append("        RENTAL INNER JOIN RENTAL_DETAIL");
		sb.append("            ON RENTAL_DETAIL.RENTAL_NUMBER = RENTAL.RENTAL_NUMBER INNER JOIN STATUS");
		sb.append("            ON RENTAL.STATUS_ID = STATUS.STATUS_ID ");
		sb.append("		LEFT JOIN STAFF AS A1 ON RENTAL.RENTAL_STAFF_ID = A1.STAFF_ID");
		sb.append("		LEFT JOIN STAFF AS A2 ON RENTAL.RETURN_STAFF_ID = A2.STAFF_ID ");
		sb.append("GROUP BY");
		sb.append("    RENTAL_NUMBER ");
		sb.append("ORDER BY");
		sb.append("    STATUS_NAME ASC");
		sb.append("    ,RENTAL_NUMBER DESC;");

		try (PreparedStatement pstmt = conn.prepareStatement(sb.toString())) {

			rs = pstmt.executeQuery();

			list = new ArrayList<>();

			while (rs.next()) {
				int num = rs.getInt(1);
				Timestamp order = rs.getTimestamp(2);
				Date returnSchedule = rs.getDate(3);
				Date returnDate = rs.getDate(4);
				int stock = rs.getInt(5);
				String rentalid = rs.getString(6);
				String returnid = rs.getString(7);
				String status = rs.getString(8);
				RentalDTO dto = new RentalDTO(num, order, returnSchedule, returnDate, stock, rentalid, returnid,
						status);
				list.add(dto);
			}

			return list;
		} catch (SQLException e) {
			e.printStackTrace();
			throw new DAOException();
		} finally {
			destroy();
		}
	}

	public ArrayList<RentalDTO> all10() throws DAOException {
		ResultSet rs = null;

		ArrayList<RentalDTO> list = null;

		init();

		StringBuffer sb = new StringBuffer();
		sb.append("SELECT");
		sb.append("        RENTAL_DETAIL.RENTAL_NUMBER");
		sb.append("        ,ORDER_DATETIME");
		sb.append("        ,RETURN_SCHEDULE");
		sb.append("        ,RETURN_DATETIME");
		sb.append("        ,COUNT(*) AS '数量'");
		sb.append("        ,A1.STAFF_NAME");
		sb.append("        ,A2.STAFF_NAME");
		sb.append("        ,STATUS_NAME");
		sb.append("    FROM");
		sb.append("        RENTAL INNER JOIN RENTAL_DETAIL");
		sb.append("            ON RENTAL_DETAIL.RENTAL_NUMBER = RENTAL.RENTAL_NUMBER INNER JOIN STATUS");
		sb.append("            ON RENTAL.STATUS_ID = STATUS.STATUS_ID ");
		sb.append("		LEFT JOIN STAFF AS A1 ON RENTAL.RENTAL_STAFF_ID = A1.STAFF_ID");
		sb.append("		LEFT JOIN STAFF AS A2 ON RENTAL.RETURN_STAFF_ID = A2.STAFF_ID ");
		sb.append("GROUP BY");
		sb.append("    RENTAL_NUMBER ");
		sb.append("ORDER BY");
		sb.append("    STATUS_NAME ASC");
		sb.append("    ,RENTAL_NUMBER DESC limit 10 offset 0;");

		try (PreparedStatement pstmt = conn.prepareStatement(sb.toString())) {

			rs = pstmt.executeQuery();

			list = new ArrayList<>();

			while (rs.next()) {
				int num = rs.getInt(1);
				Timestamp order = rs.getTimestamp(2);
				Date returnSchedule = rs.getDate(3);
				Date returnDate = rs.getDate(4);
				int stock = rs.getInt(5);
				String rentalid = rs.getString(6);
				String returnid = rs.getString(7);
				String status = rs.getString(8);
				RentalDTO dto = new RentalDTO(num, order, returnSchedule, returnDate, stock, rentalid, returnid,
						status);
				list.add(dto);
			}

			return list;
		} catch (SQLException e) {
			e.printStackTrace();
			throw new DAOException();
		} finally {
			destroy();
		}
	}

	public int searchNum() throws DAOException {
		ResultSet rs = null;

		ArrayList<RentalDTO> list = null;

		init();

		StringBuffer sb = new StringBuffer();
		sb.append("SELECT");
		sb.append("        RENTAL_DETAIL.RENTAL_NUMBER");
		sb.append("        ,ORDER_DATETIME");
		sb.append("        ,RETURN_SCHEDULE");
		sb.append("        ,RETURN_DATETIME");
		sb.append("        ,COUNT(*) AS '数量'");
		sb.append("        ,A1.STAFF_NAME");
		sb.append("        ,A2.STAFF_NAME");
		sb.append("        ,STATUS_NAME");
		sb.append("    FROM");
		sb.append("        RENTAL INNER JOIN RENTAL_DETAIL");
		sb.append("            ON RENTAL_DETAIL.RENTAL_NUMBER = RENTAL.RENTAL_NUMBER INNER JOIN STATUS");
		sb.append("            ON RENTAL.STATUS_ID = STATUS.STATUS_ID ");
		sb.append("		LEFT JOIN STAFF AS A1 ON RENTAL.RENTAL_STAFF_ID = A1.STAFF_ID");
		sb.append("		LEFT JOIN STAFF AS A2 ON RENTAL.RETURN_STAFF_ID = A2.STAFF_ID ");
		sb.append("GROUP BY");
		sb.append("    RENTAL_NUMBER ");
		sb.append("ORDER BY");
		sb.append("    STATUS_NAME ASC");
		sb.append("    ,RENTAL_NUMBER DESC");

		try (PreparedStatement pstmt = conn.prepareStatement(sb.toString())) {

			rs = pstmt.executeQuery();

			list = new ArrayList<>();

			while (rs.next()) {
				int num = rs.getInt(1);
				Timestamp order = rs.getTimestamp(2);
				Date returnSchedule = rs.getDate(3);
				Date returnDate = rs.getDate(4);
				int stock = rs.getInt(5);
				String rentalid = rs.getString(6);
				String returnid = rs.getString(7);
				String status = rs.getString(8);
				RentalDTO dto = new RentalDTO(num, order, returnSchedule, returnDate, stock, rentalid, returnid,
						status);
				list.add(dto);
			}

			return list.size();
		} catch (SQLException e) {
			e.printStackTrace();
			throw new DAOException();
		} finally {
			destroy();
		}
	}

	public ArrayList<RentalDTO> search(RentalDTO dto) throws DAOException {
		ResultSet rs = null;

		ArrayList<RentalDTO> list = null;

		init();

		StringBuffer sb = new StringBuffer();
		sb.append("SELECT");
		sb.append("        RENTAL_DETAIL.RENTAL_NUMBER");
		sb.append("        ,ORDER_DATETIME");
		sb.append("        ,RETURN_SCHEDULE");
		sb.append("        ,RETURN_DATETIME");
		sb.append("        ,COUNT(*) AS '数量'");
		sb.append("        ,RENTAL_STAFF_ID");
		sb.append("        ,RETURN_STAFF_ID");
		sb.append("        ,STATUS_NAME");
		sb.append("    FROM");
		sb.append("        RENTAL_DETAIL INNER JOIN RENTAL");
		sb.append("            ON RENTAL_DETAIL.RENTAL_NUMBER = RENTAL.RENTAL_NUMBER INNER JOIN STATUS");
		sb.append("            ON RENTAL.STATUS_ID = STATUS.STATUS_ID ");
		sb.append("WHERE");
		sb.append("    (ORDER_DATETIME LIKE ? OR ORDER_DATETIME IS NULL)");
		sb.append("    AND (RETURN_SCHEDULE LIKE ? OR RETURN_SCHEDULE IS NULL)");
		sb.append("    AND (RETURN_DATETIME LIKE ? OR RETURN_DATETIME IS NULL)");
		sb.append("    AND (RENTAL_STAFF_ID LIKE ? OR RENTAL_STAFF_ID IS NULL)");
		sb.append("    AND (RETURN_STAFF_ID LIKE ? OR RETURN_STAFF_ID IS NULL)");
		sb.append("    AND (STATUS_NAME LIKE ? OR  STATUS_NAME IS NULL) ");
		sb.append("GROUP BY");
		sb.append("    RENTAL_NUMBER ");
		sb.append("ORDER BY");
		sb.append("    STATUS_NAME ASC");
		sb.append("    ,RENTAL_NUMBER DESC;");

		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {
			ps.setString(1, dto.getsOrderDate() + "%");
			ps.setString(2, dto.getsReturnSchedule() + "%");
			ps.setString(3, dto.getsReturnDate() + "%");
			ps.setString(4, dto.getRentalid() + "%");
			ps.setString(5, dto.getReturnid() + "%");
			ps.setString(6, dto.getStatus() + "%");
			rs = ps.executeQuery();

			list = new ArrayList<>();

			while (rs.next()) {
				int num = rs.getInt(1);
				Timestamp order = rs.getTimestamp(2);
				Date returnSchedule = rs.getDate(3);
				Date returnDate = rs.getDate(4);
				int stock = rs.getInt(5);
				String rentalid = rs.getString(6);
				String returnid = rs.getString(7);
				String status = rs.getString(8);
				dto = new RentalDTO(num, order, returnSchedule, returnDate, stock, rentalid, returnid, status);
				list.add(dto);
			}

			return list;
		} catch (SQLException e) {
			throw new DAOException();
		} finally {
			destroy();
		}
	}

	public ArrayList<RentalDTO> search(RentalDTO dto, int page) throws DAOException {
		ResultSet rs = null;

		ArrayList<RentalDTO> list = null;

		init();

		StringBuffer sb = new StringBuffer();
		sb.append("SELECT");
		sb.append("        RENTAL_DETAIL.RENTAL_NUMBER");
		sb.append("        ,ORDER_DATETIME");
		sb.append("        ,RETURN_SCHEDULE");
		sb.append("        ,RETURN_DATETIME");
		sb.append("        ,COUNT(*) AS '数量'");
		sb.append("        ,A1.STAFF_NAME");
		sb.append("        ,A2.STAFF_NAME");
		sb.append("        ,STATUS_NAME");
		sb.append("    FROM");
		sb.append("        RENTAL_DETAIL INNER JOIN RENTAL");
		sb.append("            ON RENTAL_DETAIL.RENTAL_NUMBER = RENTAL.RENTAL_NUMBER INNER JOIN STATUS");
		sb.append("            ON RENTAL.STATUS_ID = STATUS.STATUS_ID ");
		sb.append("		LEFT JOIN STAFF AS A1 ON RENTAL.RENTAL_STAFF_ID = A1.STAFF_ID");
		sb.append("		LEFT JOIN STAFF AS A2 ON RENTAL.RETURN_STAFF_ID = A2.STAFF_ID ");
		sb.append(" WHERE");
		sb.append("    (ORDER_DATETIME LIKE ? OR ORDER_DATETIME IS NULL)");
		sb.append("    AND (RETURN_SCHEDULE LIKE ? OR RETURN_SCHEDULE IS NULL)");
		sb.append("    AND (RETURN_DATETIME LIKE ? OR RETURN_DATETIME IS NULL)");
		sb.append("    AND (RENTAL_STAFF_ID LIKE ? OR RENTAL_STAFF_ID IS NULL)");
		sb.append("    AND (RETURN_STAFF_ID LIKE ? OR RETURN_STAFF_ID IS NULL)");
		sb.append("    AND (STATUS_NAME LIKE ? OR  STATUS_NAME IS NULL) ");
		sb.append(" GROUP BY");
		sb.append("    RENTAL_NUMBER ");
		sb.append(" ORDER BY");
		sb.append("    STATUS_NAME ASC");
		sb.append("    ,RENTAL_NUMBER DESC limit 10 offset ?");

		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {
			ps.setString(1, dto.getsOrderDate() + "%");
			ps.setString(2, dto.getsReturnSchedule() + "%");
			ps.setString(3, dto.getsReturnDate() + "%");
			ps.setString(4, dto.getRentalid() + "%");
			ps.setString(5, dto.getReturnid() + "%");
			ps.setString(6, dto.getStatus() + "%");
			ps.setInt(7, (page - 1) * 10);
			rs = ps.executeQuery();

			list = new ArrayList<>();
			while (rs.next()) {
				int num = rs.getInt(1);
				Timestamp order = rs.getTimestamp(2);
				Date returnSchedule = rs.getDate(3);
				Date returnDate = rs.getDate(4);
				int stock = rs.getInt(5);
				String rentalid = rs.getString(6);
				String returnid = rs.getString(7);
				String status = rs.getString(8);
				dto = new RentalDTO(num, order, returnSchedule, returnDate, stock, rentalid, returnid, status);
				list.add(dto);
			}

			return list;
		} catch (SQLException e) {
			e.printStackTrace();
			throw new DAOException();
		} finally {
			destroy();
		}
	}

	public int searchNum(RentalDTO dto) throws DAOException {
		ResultSet rs = null;

		ArrayList<RentalDTO> list = null;

		init();

		StringBuffer sb = new StringBuffer();
		sb.append("SELECT");
		sb.append("        RENTAL_DETAIL.RENTAL_NUMBER");
		sb.append("        ,ORDER_DATETIME");
		sb.append("        ,RETURN_SCHEDULE");
		sb.append("        ,RETURN_DATETIME");
		sb.append("        ,COUNT(*) AS '数量'");
		sb.append("        ,RENTAL_STAFF_ID");
		sb.append("        ,RETURN_STAFF_ID");
		sb.append("        ,STATUS_NAME");
		sb.append("    FROM");
		sb.append("        RENTAL_DETAIL INNER JOIN RENTAL");
		sb.append("            ON RENTAL_DETAIL.RENTAL_NUMBER = RENTAL.RENTAL_NUMBER INNER JOIN STATUS");
		sb.append("            ON RENTAL.STATUS_ID = STATUS.STATUS_ID ");
		sb.append(" WHERE");
		sb.append("    (ORDER_DATETIME LIKE ? OR ORDER_DATETIME IS NULL)");
		sb.append("    AND (RETURN_SCHEDULE LIKE ? OR RETURN_SCHEDULE IS NULL)");
		sb.append("    AND (RETURN_DATETIME LIKE ? OR RETURN_DATETIME IS NULL)");
		sb.append("    AND (RENTAL_STAFF_ID LIKE ? OR RENTAL_STAFF_ID IS NULL)");
		sb.append("    AND (RETURN_STAFF_ID LIKE ? OR RETURN_STAFF_ID IS NULL)");
		sb.append("    AND (STATUS_NAME LIKE ? OR  STATUS_NAME IS NULL) ");
		sb.append("GROUP BY");
		sb.append("    RENTAL_NUMBER ");
		sb.append("ORDER BY");
		sb.append("    STATUS_NAME ASC");
		sb.append("    ,RENTAL_NUMBER DESC");

		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {
			ps.setString(1, dto.getsOrderDate() + "%");
			ps.setString(2, dto.getsReturnSchedule() + "%");
			ps.setString(3, dto.getsReturnDate() + "%");
			ps.setString(4, dto.getRentalid() + "%");
			ps.setString(5, dto.getReturnid() + "%");
			ps.setString(6, dto.getStatus() + "%");
			rs = ps.executeQuery();

			list = new ArrayList<>();

			while (rs.next()) {
				int num = rs.getInt(1);
				Timestamp order = rs.getTimestamp(2);
				Date returnSchedule = rs.getDate(3);
				Date returnDate = rs.getDate(4);
				int stock = rs.getInt(5);
				String rentalid = rs.getString(6);
				String returnid = rs.getString(7);
				String status = rs.getString(8);
				dto = new RentalDTO(num, order, returnSchedule, returnDate, stock, rentalid, returnid, status);
				list.add(dto);
			}

			return list.size();
		} catch (SQLException e) {
			throw new DAOException();
		} finally {
			destroy();
		}
	}

	public RentalDTO detail(int id) throws DAOException {
		RentalDTO dto = null;
		ResultSet rs = null;

		init();

		StringBuffer sb = new StringBuffer();
		sb.append("SELECT");
		sb.append("        RENTAL.RENTAL_NUMBER");
		sb.append("        ,ORDER_DATETIME");
		sb.append("        ,RETURN_SCHEDULE");
		sb.append("        ,RETURN_DATETIME");
		sb.append("        ,COUNT(*) AS '数量'");
		sb.append("        ,RENTAL_STAFF_ID");
		sb.append("        ,RETURN_STAFF_ID");
		sb.append("        ,STATUS_NAME");
		sb.append("    FROM");
		sb.append("        RENTAL_DETAIL INNER JOIN RENTAL");
		sb.append("            ON RENTAL_DETAIL.RENTAL_NUMBER = RENTAL.RENTAL_NUMBER INNER JOIN STATUS");
		sb.append("            ON RENTAL.STATUS_ID = STATUS.STATUS_ID ");
		sb.append("WHERE");
		sb.append("    RENTAL.RENTAL_NUMBER = ? ");
		sb.append("GROUP BY");
		sb.append("    RENTAL_NUMBER ");
		sb.append("ORDER BY");
		sb.append("    STATUS_NAME ASC");
		sb.append("    ,RENTAL_NUMBER DESC;");

		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {
			ps.setInt(1, id);
			rs = ps.executeQuery();

			if (rs.next()) {
				int rentalNum = rs.getInt(1);
				Timestamp order = rs.getTimestamp(2);
				Date returnSchedule = rs.getDate(3);
				Date returnDate = rs.getDate(4);
				int stock = rs.getInt(5);
				String rentalid = rs.getString(6);
				String returnid = rs.getString(7);
				String status = rs.getString(8);
				dto = new RentalDTO(rentalNum, order, returnSchedule, returnDate, stock, rentalid, returnid, status);
			}

			return dto;
		} catch (SQLException e) {
			throw new DAOException();
		} finally {
			destroy();
		}
	}

	public int rental(int rentalNum, int itemid, int identificationNum) throws DAOException {
		init();

		StringBuffer sb = new StringBuffer();
		sb.append("UPDATE");
		sb.append("        RENTAL_DETAIL");
		sb.append("    SET");
		sb.append("        IDENTIFICATION_NUMBER = ?");
		sb.append("    WHERE");
		sb.append("        RENTAL_NUMBER = ?");
		sb.append("        AND ITEM_ID = ?;");

		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {
			conn.setAutoCommit(false);

			ps.setInt(1, identificationNum);
			ps.setInt(2, rentalNum);
			ps.setInt(3, itemid);

			int result = ps.executeUpdate();

			if (result == 1) {
				conn.commit();
			}

			return result;
		} catch (SQLException e) {
			e.printStackTrace();
			try {
				conn.rollback();
			} catch (SQLException e1) {
				// TODO 自動生成された catch ブロック
				e1.printStackTrace();
			}
			throw new DAOException();
		} finally {
			destroy();
		}
	}

	public int returnItem(int rentalNum, int itemid, int identificationNum) throws DAOException {
		init();

		StringBuffer sb = new StringBuffer();
		sb.append("UPDATE");
		sb.append("        RENTAL_DETAIL");
		sb.append("    SET");
		sb.append("        RETURN_FLG = ?");
		sb.append("    WHERE");
		sb.append("        IDENTIFICATION_NUMBER = ?");
		sb.append("        AND RENTAL_NUMBER = ?");
		sb.append("        AND ITEM_ID = ?;");

		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {
			conn.setAutoCommit(false);

			ps.setBoolean(1, true);
			ps.setInt(2, identificationNum);
			ps.setInt(3, rentalNum);
			ps.setInt(4, itemid);

			int result = ps.executeUpdate();

			if (result == 1) {
				conn.commit();
			}

			return result;
		} catch (SQLException e) {
			e.printStackTrace();
			try {
				conn.rollback();
			} catch (SQLException e1) {
				// TODO 自動生成された catch ブロック
				e1.printStackTrace();
			}
			throw new DAOException();
		} finally {
			destroy();
		}
	}

	public int rentalUpdate(String status, int rentNum, String rentalid) throws DAOException {
		init();

		StringBuffer sb = new StringBuffer();
		sb.append("UPDATE");
		sb.append("        RENTAL");
		sb.append("    SET");
		sb.append("        STATUS_ID = ? ,");
		sb.append("        RENTAL_STAFF_ID = ? ,");
		sb.append("        RETURN_SCHEDULE = DATE_ADD(NOW(), INTERVAL 8 DAY) ");
		sb.append("    WHERE");
		sb.append("        RENTAL_NUMBER = ?");

		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {
			conn.setAutoCommit(false);

			ps.setString(1, status);
			ps.setString(2, rentalid);
			ps.setInt(3, rentNum);

			int result = ps.executeUpdate();

			if (result == 1) {
				conn.commit();
			}

			return result;
		} catch (SQLException e) {
			e.printStackTrace();
			try {
				conn.rollback();
			} catch (SQLException e1) {
				// TODO 自動生成された catch ブロック
				e1.printStackTrace();
			}
			throw new DAOException();
		} finally {
			destroy();
		}
	}

	public int returnUpdate(String status, int rentNum, String returnid) throws DAOException {
		init();

		StringBuffer sb = new StringBuffer();
		sb.append("UPDATE");
		sb.append("        RENTAL");
		sb.append("    SET");
		sb.append("        STATUS_ID = ?,");
		sb.append("        RETURN_DATETIME = CURRENT_DATE(),");
		sb.append("        RETURN_STAFF_ID = ?");
		sb.append("    WHERE");
		sb.append("        RENTAL_NUMBER = ?;");

		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {
			conn.setAutoCommit(false);

			ps.setString(1, status);
			ps.setString(2, returnid);
			ps.setInt(3, rentNum);
			int result = ps.executeUpdate();

			if (result == 1) {
				conn.commit();
			}

			return result;
		} catch (SQLException e) {
			e.printStackTrace();
			try {
				conn.rollback();
			} catch (SQLException e1) {
				// TODO 自動生成された catch ブロック
				e1.printStackTrace();
			}
			throw new DAOException();
		} finally {
			destroy();
		}
	}
}
