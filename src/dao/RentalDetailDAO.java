package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import dto.RentalDetailDTO;

public class RentalDetailDAO extends CommonDAO {
	public ArrayList<RentalDetailDTO> returnFlgCheck(int rentNum) throws DAOException {
		init();

		StringBuffer sb = new StringBuffer();
		sb.append("SELECT");
		sb.append("        RENTAL_NUMBER");
		sb.append("        ,ITEM_ID");
		sb.append("        ,RETURN_FLG");
		sb.append("    FROM");
		sb.append("        RENTAL_DETAIL");
		sb.append("    WHERE");
		sb.append("        RENTAL_NUMBER = ?;");

		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {
			ps.setInt(1, rentNum);
			ResultSet rs = ps.executeQuery();

			ArrayList<RentalDetailDTO> list = new ArrayList<>();

			while (rs.next()) {
				rentNum = rs.getInt(1);
				int itemid = rs.getInt(2);
				boolean returnFlg = rs.getBoolean(3);
				RentalDetailDTO dto = new RentalDetailDTO(rentNum, itemid, returnFlg);
				list.add(dto);
			}

			return list;
		} catch (SQLException e) {
			e.printStackTrace();
			throw new DAOException();
		} finally {
			destroy();
		}
	}
}
