package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import dto.GenreDTO;

public class GenreDAO extends CommonDAO{
	public static ArrayList<GenreDTO> selectGenreAllStatic() throws DAOException{
		init();

		// SQL文の作成
		StringBuffer sb = new StringBuffer();
		sb.append("SELECT");
		sb.append("        genre_id");
		sb.append("        ,genre_name");
		sb.append("        ,category_id");
		sb.append("    FROM");
		sb.append("        genre");

		// 返却用のリスト
		ArrayList<GenreDTO> genreList = new ArrayList<>();

		try(PreparedStatement ps = conn.prepareStatement(sb.toString())){

			// SQLの実行
			ResultSet rs = ps.executeQuery();

			// データを一件ずつEmpDTOの形にしてリストへ追加
			while(rs.next()){
				GenreDTO dto = new GenreDTO();
				dto.setGenre_id(rs.getInt("genre_id"));
				dto.setGenre_name(rs.getString("genre_name"));
				dto.setCategory_id(rs.getInt("category_id"));

				genreList.add(dto);
			}

		} catch (SQLException e) {
			throw new DAOException();
		}finally{
			destroy();
		}
		// リストを返す
		return genreList;
	}

	public static ArrayList<GenreDTO> selectCategoryAllStatic() throws DAOException{
		init();

		// SQL文の作成
		StringBuffer sb = new StringBuffer();
		sb.append("SELECT");
		sb.append("        category_id");
		sb.append("        ,category_name");
		sb.append("    FROM");
		sb.append("        category");

		// 返却用のリスト
		ArrayList<GenreDTO> genreList = new ArrayList<>();

		try(PreparedStatement ps = conn.prepareStatement(sb.toString())){

			// SQLの実行
			ResultSet rs = ps.executeQuery();

			// データを一件ずつEmpDTOの形にしてリストへ追加
			while(rs.next()){
				GenreDTO dto = new GenreDTO();
				dto.setCategory_id(rs.getInt("category_id"));
				dto.setCategory_name(rs.getString("category_name"));

				genreList.add(dto);
			}

		} catch (SQLException e) {
			e.printStackTrace();
			throw new DAOException();
		}finally{
			destroy();
		}
		// リストを返す
		return genreList;
	}

	public ArrayList<GenreDTO> selectGenreAll() throws DAOException{
		init();

		// SQL文の作成
		StringBuffer sb = new StringBuffer();
		sb.append("SELECT");
		sb.append("        genre_id");
		sb.append("        ,genre_name");
		sb.append("        ,genre.category_id");
		sb.append("        ,category_name");
		sb.append("    FROM");
		sb.append("        genre INNER JOIN category");
		sb.append("            ON genre.category_id = category.category_id");

		// 返却用のリスト
		ArrayList<GenreDTO> genreList = new ArrayList<>();

		try(PreparedStatement ps = conn.prepareStatement(sb.toString())){

			// SQLの実行
			ResultSet rs = ps.executeQuery();

			// データを一件ずつEmpDTOの形にしてリストへ追加
			while(rs.next()){
				GenreDTO dto = new GenreDTO();
				dto.setGenre_id(rs.getInt("genre_id"));
				dto.setGenre_name(rs.getString("genre_name"));
				dto.setCategory_id(rs.getInt("category_id"));
				dto.setCategory_name(rs.getString("category_name"));

				genreList.add(dto);
			}
			// リストを返す
			return genreList;

		} catch (SQLException e) {
			throw new DAOException();
		}finally{
			destroy();
		}
	}
}
