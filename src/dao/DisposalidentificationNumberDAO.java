package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class DisposalidentificationNumberDAO extends CommonDAO{

	public boolean disNumCheck(int id, int identNum) throws DAOException{
		init();

		PreparedStatement ps = null;

		StringBuffer sb = new StringBuffer();
		sb.append("SELECT");
		sb.append("        max_identification_number");
		sb.append("    FROM");
		sb.append("        item");
		sb.append("    WHERE");
		sb.append("        item_id = ?;");

		try{
			ps = conn.prepareStatement(sb.toString());
			ps.setInt(1, id);

			ResultSet rs = ps.executeQuery();

			int max = 0;

			if(rs.next()){
				max = rs.getInt(1);
			}

			if(identNum > 0 && identNum <= max){
				sb = new StringBuffer();
				sb.append("SELECT");
				sb.append("        *");
				sb.append("    FROM");
				sb.append("        disposal_identification_number");
				sb.append("    WHERE");
				sb.append("        item_id = ?");
				sb.append("        AND identification_number = ?;");

				ps = conn.prepareStatement(sb.toString());
				ps.setInt(1, id);
				ps.setInt(2, identNum);

				rs = ps.executeQuery();
				if(rs.next()){
					return false;
				}

				sb = new StringBuffer();
				sb.append("SELECT");
				sb.append("        *");
				sb.append("    FROM");
				sb.append("        rental_detail");
				sb.append("    WHERE");
				sb.append("        item_id = ?");
				sb.append("        AND identification_number = ?;");

				ps = conn.prepareStatement(sb.toString());

				ps.setInt(1, id);
				ps.setInt(2, identNum);

				rs = ps.executeQuery();
				if(rs.next()){
					return false;
				}

				return true;
			}else{
				return false;
			}
		}catch(SQLException e){
			e.printStackTrace();
			return false;
		}finally{
			destroy();
		}
	}

}
